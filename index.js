import InteractiveImageCanvas from './src';

import Zoomer from './src/modules/zoomer/zoomer';
import PincherPanner from './src/modules/pincher-panner/PincherPanner';

import Scroller from './src/modules/scroller/scroller';
import DraggableScrollerBehavior from './src/modules/scroller/draggable-scroller-behavior';
import KeyboardScrollerBehavior from './src/modules/scroller/keyboard-scroller-behavior';

import Annotator from './src/modules/annotator/annotator';
import OverlayUI from './src/modules/ui/overlay-ui';

import ViewTool from './src/tools/view-tool';
import Toolbox from './src/tools/toolbox';

class InteractiveImage {
  constructor(data, imageUrl, parentNode) {
    // Load the image itself; onload needs to be defined before the src is set
    var img = new Image();
    img.parentContext = this;
    img.onload = function() {
      this.parentContext.loadCanvas(
        data.json,
        this,
        parentNode,
      );
    };
    img.src = imageUrl;
  }

  loadCanvas(data, img, parentNode) {
    if (this.ii) {
      this.ii.destroy();
    }

    // Initialize InteractiveImageCanvas lib
    this.ii = new InteractiveImageCanvas();
    const { ii } = this;
    ii.setImg(img);

    // Add zoomer
    const zoomer = new Zoomer(ii);
    ii.addZoomer(zoomer);
    ii.zoomer.setZoom(data.scale);

    // Add scrollers
    const scroller = new Scroller();
    scroller.addBehavior(new DraggableScrollerBehavior(ii.canvas));

    // Add interactive behaviors
    const keyboardBehavior = new KeyboardScrollerBehavior();
    scroller.addBehavior(keyboardBehavior);
    keyboardBehavior.init();
    ii.addScroller(scroller);

    // This has to be called after scroller is added
    ii.setSize(parentNode.offsetWidth, parentNode.offsetHeight);

    // Add annotations
    const annotator = new Annotator(data.dots, ii);
    ii.addAnnotator(annotator);

    // Add view tool
    const toolbox = new Toolbox(ii);
    toolbox.addTool(new ViewTool('VIEW', null, annotator, ii));
    toolbox.selectTool('VIEW');

    ii.addPincherPanner(new PincherPanner(ii));

    // Add canvas
    parentNode.appendChild(ii.canvas);

    // UI at the bottom of the image (must be done after canvas added)
    ii.addUI(new OverlayUI(ii));

    // Basially sets to max zoom
    ii.zoomer.setZoom(0.05);
  }

  onWindowsResize(newWidth, newHeight) {
    if (this.ii) {
      const canvas = document.getElementsByTagName('canvas')[0];

      canvas.style.display = 'none';
      this.ii.setSize(newWidth, newHeight);
      canvas.style.display = 'block';

      if (this.ii.zoomer) {
        this.ii.zoomer.checkMaxZoom();
      }
    }
  }

  destroy() {
    if (this.ii) {
      this.ii.destroy();
      this.ii = null;
    }
  }
}

export default InteractiveImage;
