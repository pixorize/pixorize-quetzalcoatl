# Pixorize Quetzalcoatl
This is the code base of the interactive image tool used in the
[view](https://pixorize.com/view/3680) page of the Pixorize website.


## Inspiration
[Quetzalcoatl](https://en.wikipedia.org/wiki/Quetzalcoatl) is considered
one of the most important deity in Mesoamerican culture, he's recognized
as the god of **life**, light and wisdom, the same as Interactive images
give **life** to Pixorize.


## Instalation
```shell
npm install @pixorize/pixorize-quetzalcoatl
```
or
```shell
yarn add @pixorize/pixorize-quetzalcoatl
```


## Usage
```javascript
import PixorizeQuetzalcoatl from '@pixorize/pixorize-quetzalcoatl';

const data = {};
const imageUrl = 'https://via.placeholder.com/1280x720';
const parentNode = document.querySelector('#parent-node');

const interactiveImageLib = new PixorizeQuetzalcoatl(data, imageUrl, parentNode);
```


## Documentation

### Properties
`@pixorize/pixorize-quetzalcoatl` exposes a JavaScript class which is responsible of _drawing_
the interactive image (`canvas` element and all its interaction).
The class' constructor takes 3 parameters:

`data`: `JSON`
The interactive image JSON data. There is one and only required property on this JSON
which is simply called `json`, which should contain 2 properties:
`scale` and `dots`. `scale` is the default zoom scale of the interactive image,
and `dots` is the collection (array) of dots within the image. `data` should look like this:
```json
{
  "json": {
    "scale": "1",
    "dots": [
      {
        "x": 725,
        "y": 623.5,
        "text": {
          "ops": [
            {
              "attributes": {
                "bold": true
              },
              "insert": "Component of TPP"
            },
            {
              "attributes": {
                "align": "center",
                "header": 2
              },
              "insert": "\n"
            },
            {
              "insert": "(TPP teepees)"
            },
            {
              "attributes": {
                "align": "center"
              },
              "insert": "\n"
            },
            {
              "insert": "\nThiamine is a component of the cofactor "
            },
            {
              "attributes": {
                "bold": true
              },
              "insert": "TPP"
            },
            {
              "insert": "\n"
            }
          ]
        },
        "type": "DOT"
      }
    ]
  }
}
```

`imageUrl`: `string`
The image URL.

`parentNode`: `HTMLElement`
The parent HTML element/node in which the interactive image will be inserted in.
It is **important** to know that the parent node must have a valid width and height
for the interactive image to be loaded. This library uses parent node's
`offsetWidth` and `offsetHeight` values to display the `canvas`.

### Instance methods

Once the class is instantiated, it exposes two useful methods.

`onWindowsResize`: `void(newWidth: number, newHeight: number)`
Useful for having a responsive interactive image. This methods dynamically updates
the `width` and `height` of the image based on the passed parameters.
Here is common to pass parent node's new width and height.

`destroy`: `void()`
Fully destroys the interactive image from the DOM (parent node still remains in the DOM).


---


## License
[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)

- [MIT license](https://bitbucket.org/pixorize/pixorize-quetzalcoatl/src/master/LICENSE.md)
- Copyright 2020 © [Pixorize Inc.](https://pixorize.com/)
