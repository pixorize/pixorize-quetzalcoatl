import $ from 'jquery';
const QuillDeltaToHtmlConverter = require('quill-delta-to-html').QuillDeltaToHtmlConverter;

import Tool from './tool';
import Tooltip from '../lib/tooltip';

import LayerManager from '../manager/layer-manager';

var ii;
var viewToolHasFocus = true;

var count = 0;

var hey;

var ViewTool = function(name, selector, annotator, interactiveImage) {
  count ++;
  this.count = count;

	ii = interactiveImage;

	Tool.call(this, name, selector, annotator, interactiveImage, []); // super();

	this.hoveredAnnotation = null;
	this.selectedAnnotation = null;
  this.tooltip = new Tooltip('#interactive-image');

  this.lastClickPos = null;

	this.layerManager = new LayerManager();


	var isMobile = /Mobi/.test(navigator.userAgent);
	if(isMobile) {
		$(interactiveImage.canvas).on("touchstart", function() {
			this.tooltip.hide();

			if(this.hoveredAnnotation) {
				this.__dehoverAnnotation(this.hoveredAnnotation);
			}
		}.bind(this));
	}

  var parent = this;
  $(interactiveImage.canvas).on("mouseover", function(e) {
    parent.hoverCanvas = true;
  });

  $(this.tooltip.elem).on("mouseover", function(e) {
    parent.hoverTooltip = true;
  });

  $(interactiveImage.canvas).on("mouseout", function(e) {
    parent.hoverCanvas = false;
  });

  $(this.tooltip.elem).on("mouseout", function(e) {
    parent.hoverTooltip = false;
  });

  hey = this;
};

var hasFocus = function() {
  return hey.hoverCanvas || hey.hoverTooltip;
};

ViewTool.prototype.__onCanvasMouseOver = function(e) {
  console.log('eh');
};

ViewTool.prototype = Object.create(Tool.prototype);
ViewTool.prototype.constructor = Tool;

ViewTool.prototype.draw = function(context) {
}

ViewTool.prototype.__hoverAnnotation = function(annotation) {
	if(this.hoveredAnnotation == annotation) return;
  //if(this.selectedAnnotation) return;

	this.hoveredAnnotation = annotation;
	annotation.hover();
	// $(this).trigger("change");
}

ViewTool.prototype.__dehoverAnnotation = function(annotation) {
	if(!annotation.isHovered) return;

	this.hoveredAnnotation = null;
	annotation.dehover();
	// $(this).trigger("change");
}

ViewTool.prototype.__showAnnotationToolTip = function(imageMousePos, annotation) {
  var pos;

  var offset = $(this.interactiveImage.canvas).offset();
  var leftSub = offset.left;
  var topSub = offset.top;
  leftSub = 0;
  topSub = 0;

  imageMousePos.x -= leftSub;
  imageMousePos.y -= topSub;

  if(annotation.type == "DOT") {
    pos = {x: annotation.pos.x - leftSub, y: annotation.pos.y - topSub + annotation.radius};
  } else {
    if(this.selectedAnnotation) {
      pos = this.lastClickPos;
    } else {
      pos = imageMousePos;
    }
  }

  var transPos = ii.imageToPageCoords(pos.x, pos.y);

  var text = annotation.text;
  if(typeof text == "string") {
    // quill.setText(annotation.text);
  } else {
    var converter = new QuillDeltaToHtmlConverter(text.ops, {});

    converter.afterRender(function(a, html){
        html = html.replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;');
        html = html.replace(/  /g, '&nbsp;&nbsp;');

        return html;
    });

    var html = converter.convert();
    text = converter.convert();
  }

  if(!this.selectedAnnotation) this.tooltip.show(transPos.x, transPos.y, text);
};

ViewTool.prototype.onMouseOut = function(e) {
  if(this.hoveredAnnotation) {
		this.__dehoverAnnotation(this.hoveredAnnotation);
  }

	if(!this.selectedAnnotation) this.tooltip.hide();
}

ViewTool.prototype.onCanvasMouseDown = function(e, imageMousePos) {
  if(this.selectedAnnotation) {
    this.selectedAnnotation.deselect();
    this.selectedAnnotation = null;
  }

  if(this.hoveredAnnotation) {
    this.lastClickPos = imageMousePos;

    this.__showAnnotationToolTip(imageMousePos, this.hoveredAnnotation);
  	this.selectedAnnotation = this.hoveredAnnotation;
  	this.selectedAnnotation.select();
  }

  // remove later
  //if(this.selectedAnnotation) this.selectedAnnotation.deselect();
  //this.selectedAnnotation = null;
  // remove later

  if(!this.hoveredAnnotation && !this.selectedAnnotation) this.tooltip.hide();
	$(this).trigger("change");
}

ViewTool.prototype.onImageMouseMove = function(imageMousePos, imageBounds, interactiveImage) {
  if(this.count != count) {
    console.log("MULTIPLE INSTANCES ACTIVE");
    return;
  }

  if(!hasFocus()) {
    if(this.hoveredAnnotation) {
      this.__dehoverAnnotation(this.hoveredAnnotation);
    }
    this.tooltip.hide();
    return;
  }

  var posAbs = interactiveImage.imageToCanvasCoords(imageMousePos.x, imageMousePos.y);
  var bounds = interactiveImage.getBounds();

  var inBounds = true;
  if(!(posAbs.x <= bounds.width && posAbs.x >= 0)) {
    inBounds = false;
  }

  if(!(posAbs.y <= bounds.height && posAbs.y >= 0)) {
    inBounds = false;
  }

  if(!inBounds) {
    $.each(this.annotator.annotations, function(i, annotation) {
	    this.__dehoverAnnotation(annotation);
    }.bind(this));

    if(!this.selectedAnnotation) {
      this.hoveredAnnotation = null;
      this.tooltip.hide();
    }

    return;
  }
 
  if(!viewToolHasFocus) return;

	if(!this.oldImageMousePos) {
		this.oldImageMousePos = {x: 0, y: 0};
	}

  var hoveredAnnotation = null;

	var reversed = _.clone(this.annotator.annotations);
	reversed.reverse();
	this.layerManager.sort(reversed);

	var _hovered = []

	$.each(reversed, function(i, annotation) {
    if(annotation.pointWithinObj(imageMousePos)) {
      hoveredAnnotation = annotation;
			_hovered.push(annotation);
			// this.__hoverAnnotation(annotation);
  	} else {
	    this.__dehoverAnnotation(annotation);
		}
  }.bind(this));

  var annotation;

  if(hoveredAnnotation) {
    annotation = hoveredAnnotation;
    this.__hoverAnnotation(annotation);

		$.each(_hovered, function(i, a) {
			if(a != hoveredAnnotation) this.__dehoverAnnotation(a);
		}.bind(this));
  } else if(this.selectedAnnotation) {
    annotation = this.selectedAnnotation;
  }

  if(annotation) {
    this.__showAnnotationToolTip(imageMousePos, annotation);
  }
  //if(annotation) {
    //var pos;

    //if(annotation.type == "DOT") {
      //pos = {x: annotation.pos.x, y: annotation.pos.y + annotation.radius};
    //} else {
      //if(this.selectedAnnotation) {
        //pos = this.lastClickPos;
      //} else {
        //pos = imageMousePos;
      //}
    //}

    //var transPos = ii.imageToPageCoords(pos.x, pos.y);

		//var text = annotation.text;
		//if(typeof text == "string") {
			//// quill.setText(annotation.text);
		//} else {
			//var converter = new QuillDeltaToHtmlConverter(text.ops, {});

			//converter.afterRender(function(a, html){
					//html = html.replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;');
					//html = html.replace(/  /g, '&nbsp;&nbsp;');

					//return html;
			//});

			//var html = converter.convert();
			//text = converter.convert();
		//}

		//if(!this.selectedAnnotation) this.tooltip.show(transPos.x, transPos.y, text);
  //}

  if(!hoveredAnnotation && !this.selectedAnnotation) {
    this.tooltip.hide();
  }

	this.oldImageMousePos = imageMousePos;
}

export default ViewTool;
