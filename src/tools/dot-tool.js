import $ from 'jquery';
import Tool from './tool';

var DotTool = function(name, selector, annotator, interactiveImage) {
	// d=68 (chrome, firefox)
	Tool.call(this, name, selector, annotator, interactiveImage, [68]); // super();

	this.imageX = null;
	this.imageY = null;
  this.__pointDrawer = new PointDrawer();
};

DotTool.prototype = Object.create(Tool.prototype);
DotTool.prototype.constructor = Tool;

DotTool.prototype.draw = function(context) {
	$("body").css("cursor", "crosshair");

	if(this.imageX != null) {
		var canvasPos = this.interactiveImage.imageToCanvasCoords(this.imageX, this.imageY);
		this.__pointDrawer.drawPoint(context, canvasPos.x, canvasPos.y, false, true);
	}
}

DotTool.prototype.onImageMouseMove = function(imageMousePos, imageBounds) {
	this.imageX = imageMousePos.x;
	this.imageY = imageMousePos.y;
	$(this).trigger("change");
}

DotTool.prototype.onCanvasMouseDown = function(e, imagePoint) {
	e.stopImmediatePropagation();
	var annotation = new PointAnnotation(imagePoint.x, imagePoint.y, "");
	this.annotator.addAnnotation(annotation);
};

export default DotTool;
