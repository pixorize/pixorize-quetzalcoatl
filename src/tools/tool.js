import $ from 'jquery';

var Tool = function(name, selector, annotator, interactiveImage, hotkey) {
  this.name = name;
  this.selector = selector;
  this.annotator = annotator;
  this.interactiveImage = interactiveImage;
  this.hotkey = hotkey;
};

Tool.prototype.activate = function() {
  $(this).trigger("change");
};

Tool.prototype.deactivate = function() {
  $(this).trigger("change");
};

Tool.prototype.draw = function(context) {
};

Tool.prototype.onMouseOut = function(e) {
};

Tool.prototype.onCanvasDblClick = function(imagePoint) {
};

Tool.prototype.onCanvasMouseDown = function(imageMousePos) {
};

Tool.prototype.onDocumentMouseUp = function(imageMousePos) {
};

Tool.prototype.onImageMouseMove = function(imageMousePos, imageBounds) {
};

Tool.prototype.onDocumentKeyPress = function(imageMousePos, imageBounds) {
};

export default Tool;
