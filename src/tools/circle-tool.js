import $ from 'jquery';
import Tool from './tool';

var CircleTool = function(name, selector, annotator, interactiveImage) {
	// c=99 (chrome), 67 (firefox)
	Tool.call(this, name, selector, annotator, interactiveImage, [99, 67]); // super();

	this.isDrawing = false;
	this.imageX = null;
	this.imageY = null;
	this.startDrawX = null;
	this.startDrawY = null;

  this.__drawer = new Drawer();
	this.__pointDrawer = new PointDrawer();

	this.SELECTED_STROKE_STYLE = "rgb(178, 241, 130)";
};

CircleTool.prototype = Object.create(Tool.prototype);
CircleTool.prototype.constructor = Tool;

CircleTool.prototype.draw = function(context) {
	$("body").css("cursor", "crosshair");

	var canvasPos = this.interactiveImage.imageToCanvasCoords(this.imageX, this.imageY);

	if(this.isDrawing) {
		var origCanvasPos = this.interactiveImage.imageToCanvasCoords(this.startDrawX, this.startDrawY);
		var origPosVec = new Vector2(origCanvasPos.x, origCanvasPos.y);
		var posVec = new Vector2(canvasPos.x, canvasPos.y);
		var dist = origPosVec.distance(posVec);
		var diff = posVec.subtract(origPosVec);

		this.__drawer.context = this.interactiveImage.context;

		this.__drawer.drawLineSegment(
			origPosVec, posVec,
			{strokeStyle: this.SELECTED_STROKE_STYLE, lineWidth: 3}
		);

		var	outerStrokeStyle = "rgba(20, 119, 65, 1)";
		var	innerStrokeStyle = "rgba(178, 241, 130, 1)";

	  this.__drawer.drawDoubleCircle(
			canvasPos.x-diff.x, canvasPos.y-diff.y,
			dist,
			{strokeStyle: outerStrokeStyle, lineWidth: 3},
			{strokeStyle: innerStrokeStyle, lineWidth: 1}
		);

		this.__pointDrawer.drawPointScaled(context, origPosVec.x, origPosVec.y, false, true, .75);
		this.__pointDrawer.drawPointScaled(context, canvasPos.x, canvasPos.y, false, true, .75);
	} else {
		if(this.imageX != null) {
			// this.__dotDrawer.drawDot(context, canvasPos.x, canvasPos.y, false, false);
		}
	}
}

CircleTool.prototype.deactivate = function() {
	this.stopDraw();
};

CircleTool.prototype.stopDraw = function() {
	if(this.startDrawX == null) return;
	this.startDrawX = null;
	this.startDrawY = null;
	this.isDrawing = false;
	$(this).trigger("change");
}

CircleTool.prototype.onDocumentKeyPress = function(e) {
	if(e.keyCode == 127 || e.keyCode == 27) { // delete and esc keys (i.e. cancel)
    this.stopDraw();
	}
}

CircleTool.prototype.onImageMouseMove = function(imageMousePos, imageBounds) {
	this.imageX = imageMousePos.x;
	this.imageY = imageMousePos.y;

	if(this.isDrawing) $(this).trigger("change");
}

CircleTool.prototype.onCanvasMouseDown = function(e, imagePoint) {
	this.startDrawX = imagePoint.x;
	this.startDrawY = imagePoint.y;
	this.isDrawing = true;

	e.stopImmediatePropagation();
};

CircleTool.prototype.onDocumentMouseUp = function(e, imagePoint) {
	if(this.startDrawX == null) return;

	var startDrawVec = new Vector2(this.startDrawX, this.startDrawY);
	var endDrawVec = new Vector2(this.imageX, this.imageY);
	var radius = startDrawVec.distance(endDrawVec);

	var annotation = new CircleAnnotation(startDrawVec.x, startDrawVec.y, radius, "");
	this.annotator.addAnnotation(annotation);

	this.startDrawX = null;
	this.startDrawY = null;
	this.isDrawing = false;
	$(this).trigger("change");
};

export default CircleTool;
