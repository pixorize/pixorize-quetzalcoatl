import $ from 'jquery';
import Tool from './tool';

var RectTool = function(name, selector, annotator, interactiveImage) {
	// r=114 (chrome), 82 (firefox)
	Tool.call(this, name, selector, annotator, interactiveImage, [114, 82]); // super();

  this.imageMousePos = null;
  this.startRectPos = null;

	this.__polyDrawer = new PolyDrawer();
};

RectTool.prototype = Object.create(Tool.prototype);
RectTool.prototype.constructor = Tool;

RectTool.prototype.draw = function(context, canvasPos, interactiveImage) {
	$("body").css("cursor", "crosshair");

  // PolyDrawer.prototype.drawPoly(context, poly, lineColor, pointFillColor, pointStrokeColor);

  if(this.startRectPos != null) {
    if(this.imageMousePos != null) {
      var points = this.getPoly();

      var pointsT = [];
      $.each(points, function(i, point) {
        var transformed = interactiveImage.imageToCanvasCoords(point.x, point.y);
        pointsT.push(new Vector(transformed.x, transformed.y));
      });

      this.__polyDrawer.drawPoly(context, pointsT, false, true);
    }
  }
}

RectTool.prototype.getPoly = function() {
  var w = this.imageMousePos.x - this.startRectPos.x;
  var h = this.imageMousePos.y - this.startRectPos.y;

  var points = [];
  points.push(new Vector(this.startRectPos.x, this.startRectPos.y));
  points.push(new Vector(this.startRectPos.x, this.startRectPos.y + h));
  points.push(new Vector(this.startRectPos.x + w, this.startRectPos.y + h));
  points.push(new Vector(this.startRectPos.x + w, this.startRectPos.y));
  return points;
}

RectTool.prototype.onImageMouseMove = function(imageMousePos, imageBounds) {
  this.imageMousePos = imageMousePos;
	if(this.startRectPos != null) $(this).trigger("change");
}

RectTool.prototype.onCanvasMouseDown = function(e, imagePoint) {
  e.stopImmediatePropagation();
  this.startRectPos = imagePoint;
};

RectTool.prototype.deactivate = function() {
	this.stopDraw();
};

RectTool.prototype.stopDraw = function() {
  if(this.startRectPos == null || this.imageMousePos == null) return;
  this.startRectPos = null;
  this.imageMousePos = null;
	$(this).trigger("change");
}

RectTool.prototype.onDocumentKeyPress = function(e) {
	if(e.keyCode == 127 || e.keyCode == 27) { // delete and esc keys (i.e. cancel)
    this.stopDraw();
	}
}

RectTool.prototype.onDocumentMouseUp = function(e, imagePoint) {
  if(this.startRectPos == null || this.imageMousePos == null) return;

  var points = this.getPoly();
	var poly = new PolyAnnotation(points, "");
	this.annotator.addAnnotation(poly);

  this.startRectPos = null;
  this.imageMousePos = null;
	$(this).trigger("change");
};

export default RectTool;
