import $ from 'jquery';
import Tool from './tool';

var ii;

var SelectTool = function(name, selector, annotator, interactiveImage) {
	ii = interactiveImage;

	// v=118 (chrome), 86 (firefox)
	Tool.call(this, name, selector, annotator, interactiveImage, [118, 86]); // super();

	this.hoveredAnnotation = null;
	this.selectedAnnotation = null;
  this.selectedBBDrawer = new SelectedBBDrawer();
  this.__deleteIsHovered = false;
	this.__isDragging = false;
	this.layerManager = new LayerManager();

	shortcut.add("Ctrl+Shift+A", function() {
		if(this.selectedAnnotation) this.__deselectAnnotation(this.selectedAnnotation);
		this.selectedAnnotation = null;
	}.bind(this));
};

SelectTool.prototype = Object.create(Tool.prototype);
SelectTool.prototype.constructor = Tool;

SelectTool.prototype.deactivate = function() {
	if(this.selectedAnnotation) this.__deselectAnnotation(this.selectedAnnotation);
	if(this.hoveredAnnotation) this.hoveredAnnotation.dehover();

	this.selectedAnnotation = null;
	this.hoveredAnnotation = null;

  this.__deleteIsHovered = false;
	this.__isDragging = false;
  Tool.prototype.deactivate.call(this);  // super.deactivate();
}

SelectTool.prototype.onDocumentKeyPress = function(e) {
	if(e.keyCode == 127 || e.keyCode == 46) { // delete key (i.e. cancel)
		if(this.selectedAnnotation) {
			var toDelete = this.selectedAnnotation;
			this.__deselectAnnotation(this.selectedAnnotation);
			this.annotator.deleteAnnotation(toDelete);
			this.hoveredAnnotation = null;
			$("body").css("cursor", "default");
		}
	}
}

SelectTool.prototype.draw = function(context) {
	$("body").css("cursor", "default");
	if(this.hoveredAnnotation || this.__isDragging) $("body").css("cursor", "move");

	if(this.selectedAnnotation) {
		var rect = this.selectedAnnotation.getBB();
		var canvasPos = this.interactiveImage.imageToCanvasCoords(rect.x, rect.y);
		rect.x = canvasPos.x;
		rect.y = canvasPos.y;

		this.selectedBBDrawer.drawBB(context, rect, this.__deleteIsHovered);
	}

	if(this.__deleteIsHovered) {
		$("body").css("cursor", "pointer");
	}
}

SelectTool.prototype.__hoverAnnotation = function(annotation) {
  console.log('chuh');
	if(this.hoveredAnnotation == annotation) return;

	this.hoveredAnnotation = annotation;
	annotation.hover();
	$(this).trigger("change");
}

SelectTool.prototype.__dehoverAnnotation = function(annotation) {
	if(!annotation.isHovered || !this.hoveredAnnotation) return;

	this.hoveredAnnotation = null;
	annotation.dehover();
	$(this).trigger("change");
}

SelectTool.prototype.__selectAnnotation = function(annotation) {
	if(this.selectedAnnotation == annotation) return;
	if(this.selectedAnnotation) this.__deselectAnnotation(this.selectedAnnotation, true);

	this.selectedAnnotation = annotation;
	annotation.select();

	// move the annotation to the top of the annotator's list
	// (draw it on top of everything)
	this.annotator.deleteAnnotation(annotation);
	this.annotator.addAnnotation(annotation, true);

	$(this).trigger("change");
	$(this).trigger("ACTIVE_ANNOTATION_CHANGE", annotation);
}

SelectTool.prototype.__deselectAnnotation = function(annotation, doSuppressEvent) {
	if(!annotation || !annotation.isSelected) return;

	annotation.deselect();
	this.selectedAnnotation = null;
	$(this).trigger("change");
	if(!doSuppressEvent) $(this).trigger("ACTIVE_ANNOTATION_CHANGE", false);
}

SelectTool.prototype.__startAnnotationDrag = function(annotation) {
	var wasDragging = this.__isDragging;
	annotation.startDrag();
	this.__isDragging = true;
	if(this.__isDragging != wasDragging) $(this).trigger("change");
}

SelectTool.prototype.__handleAnnotationDrag = function(imageMousePos, oldImageMousePos, imageBounds, interactiveImage) {
	if(this.__isDragging) {
		// var lastPos = {x: this.selectedAnnotation.pos.x, y: this.selectedAnnotation.pos.y};

    var newX = _.clamp(imageMousePos.x, 0, imageBounds.width);
    var newY = _.clamp(imageMousePos.y, 0, imageBounds.height);

		var dx = imageMousePos.x - oldImageMousePos.x;
		var dy = imageMousePos.y - oldImageMousePos.y;

		if(this.selectedAnnotation.type == "DOT" || this.selectedAnnotation.type == "CIRCLE") {
			this.selectedAnnotation.addPosition(dx, dy);
			// this.selectedAnnotation.setPosition(newX, newY);
		} else {
			this.selectedAnnotation.addPosition(dx, dy);
		}

		// this.selectedAnnotation.onDrag(lastPos);
    $(this).triggerHandler("change");
  }
}

SelectTool.prototype.onCanvasMouseDown = function(e, imageMousePos) {
	var stopProp = false;
	var selectedAnnotation = null;
	var wasSelectedAnnotation = this.selectedAnnotation;
	var toDelete;

  $.each(this.annotator.annotations, function(i, annotation) {
		if(!annotation) return; // continue if previous element was deleted via click event

		if(this.__deleteIsHovered) {
			toDelete = wasSelectedAnnotation;
			this.__deselectAnnotation(wasSelectedAnnotation);
		} else {
	    if(!selectedAnnotation && annotation == this.hoveredAnnotation) {
				this.__selectAnnotation(annotation);
				selectedAnnotation = annotation;

				this.__startAnnotationDrag(annotation);
				stopProp = true;
	    } else {
				if(selectedAnnotation != annotation) this.__deselectAnnotation(annotation);
			}
		}
  }.bind(this));

	if(toDelete) {
		this.annotator.deleteAnnotation(toDelete); // move it outside loop so no problems
		$("body").css("cursor", "default");
	}
	this.selectedAnnotation = selectedAnnotation;
// 	if((wasSelectedAnnotation != selectedAnnotation) && (wasSelectedAnnotation && selectedAnnotation)) {
// 		this.__deselectAnnotation(wasSelectedAnnotation); // so the event fires
// //HERE HERE HRE
// 	}
	$(this).trigger("change");

  if(stopProp) e.stopImmediatePropagation();
}

SelectTool.prototype.onImageMouseMove = function(imageMousePos, imageBounds, interactiveImage) {
	if(!this.oldImageMousePos) {
		this.oldImageMousePos = {x: 0, y: 0};
	}

	var hovered = null;

	if(!this.__isDragging) {
		var reversed = _.clone(this.annotator.annotations);
		reversed.reverse();
		this.layerManager.sort(reversed);

	  $.each(reversed, function(i, annotation) {
			// this.__dehoverAnnotation(annotation);

			if(annotation.pointWithinObj(imageMousePos)) {
				hovered = annotation;
	  	} else {
				this.__dehoverAnnotation(annotation);
	  	}
	  }.bind(this));

		if(hovered) {
			this.__hoverAnnotation(hovered);
		}
	}

	var deleteWasHovered = this.__deleteIsHovered;
	this.__deleteIsHovered = this.__checkIfDeleteIsHovered(imageMousePos);
	if(deleteWasHovered != this.__deleteIsHovered) {
		 $(this).trigger("change");
	}


	this.__handleAnnotationDrag(imageMousePos, this.oldImageMousePos, imageBounds, interactiveImage);
	this.oldImageMousePos = imageMousePos;
}

SelectTool.prototype.onDocumentMouseUp = function(imageMousePos) {
	this.__isDragging = false;
};

SelectTool.prototype.__checkIfDeleteIsHovered = function(imageMousePos) {
	if(this.selectedAnnotation == null) return false;

	var rect = this.selectedAnnotation.getBB();
  var deletePos = this.selectedBBDrawer.getDeleteButtonPos(rect);

  if(imageMousePos.vectorWithinDist(deletePos, this.selectedBBDrawer.DELETE_RADIUS)) {
    return true;
  } else {
		return false;
	}
}

export default SelectTool;
