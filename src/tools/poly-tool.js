import $ from 'jquery';
import Tool from './tool';

var DEBUG_DRAW;
var DEBUG_CONTEXT;
var DEBUG_TRANSFORM_FUNCTION; // fxn used to transform points from one coordinate system to canvas coordinate system
var DEBUG_DRAWER;

var POLY_TOOL_DEBUG = false;

var transformFunction;

var PolyTool = function(name, selector, annotator, interactiveImage) {
	DEBUG_DRAW = true;
	DEBUG_CONTEXT = interactiveImage.context;
	DEBUG_DRAWER = new Drawer(DEBUG_CONTEXT);

	this.transformFunction = function(point) {
		var transformedPoint = interactiveImage.imageToCanvasCoords(point.x, point.y);
		return new Vector2(transformedPoint.x, transformedPoint.y);
	}

	DEBUG_TRANSFORM_FUNCTION = transformFunction;

	// b=98 (chrome), 66 (firefox)
	Tool.call(this, name, selector, annotator, interactiveImage, [98, 66]); // super();

	this.pointRadius = 5;
  this.snapRadius = 30;

	polyResetVars(this);

	this.__polyDrawer = new PolyDrawer();
	this.__drawer = new Drawer();
};

function polyResetVars(p) {
	p.draggedPoint = null;
	p.draggedAnnotation = null;
  p.isDrawing = false;
  p.isColliding = false;
  p.lastPoint = null;

  p.isSnapped = false;
  p.snapPoint = null;

  p.activeShape = null;

  p.currentPoly = [];
  p.shapes = [];
  p.hoverShapes = [];

	p.canReleaseDrag = true;

  // var triangle = [
  //   {x: 100, y: 100},
  //   {x: 150, y: 100},
  //   {x: 150, y: 150},
  //   {x: 200, y: 100},
  //   {x: 150, y: 50},
  //   {x: 150, y: 70}
  // ];
	//
  // p.addShape(triangle);


	/*
	* New stuff
	*/
	// The points currently being drawn
	p.__pointsBeingDrawn = [];
};

PolyTool.prototype = Object.create(Tool.prototype);
PolyTool.prototype.constructor = Tool;

PolyTool.prototype.deactivate = function() {
	polyResetVars(this);
  $(this).trigger("change");
};

/*
* shapes
*/

PolyTool.prototype.addShape = function(shape) {
	var poly = new PolyAnnotation(shape, "");
	this.annotator.addAnnotation(poly);
}

PolyTool.prototype.draw = function(context, canvasPos, interactiveImage) {
	console.log("PolyTool draw")

	$("body").css("cursor", "crosshair");
	if(this.hoveredPoint != null || this.draggedPoint != null) {
		$("body").css("cursor", "move");
		return;
	}

	if(!this.isDrawing && this.draggedPoint == null) {
		var canvasPos = interactiveImage.imageToCanvasCoords(this.imageMouseX, this.imageMouseY);
    this.__polyDrawer.drawFirstVertex(context, canvasPos.x, canvasPos.y, "black");
	}

	/*
	* Draw the (incomplete) poly that's currently being drawn
	*/

	this.isColliding = false;

	var toDrawPoints = _.clone(this.__pointsBeingDrawn);

	// Check for intersections b/w the line currently being drawn and previously drawn lines
	if(this.isDrawing) {
		var lastPointDrawn = this.__pointsBeingDrawn[this.__pointsBeingDrawn.length-1];
		var lastMouseImagePoint = new Vector2(this.imageMouseX, this.imageMouseY);

		var firstPointDrawn = this.__pointsBeingDrawn[0];
		var pointBeingDrawn = this.__attemptSnapPoint(lastMouseImagePoint, firstPointDrawn, this.snapRadius);

		this.isSnapped = false;
		if(_.isEqual(pointBeingDrawn, firstPointDrawn)) {
			this.isSnapped = true;
			this.snapPoint = firstPointDrawn;
		}

		var lineBeingDrawn = new LineSegment(lastPointDrawn, pointBeingDrawn);
		var previousLinesDrawn = this.__calculateLinesFromPoints(this.__pointsBeingDrawn);

		var intersections = this.__calculateIntersectionsOfLineWithLines(lineBeingDrawn, previousLinesDrawn);
		// Allow intersections at the first point (which snaps and closes shape)
		// and at the last point (from which we are drawing)
		var allowedIntersections = [firstPointDrawn, lastPointDrawn];
		var threshold = .1;
		intersections = this.__cleanPointsWithinDistance(intersections, allowedIntersections, threshold);

		if(intersections.length > 0) {
			this.isColliding = true;
		}

		var color = "black";
		if(this.isColliding) color = "red";

		/*
		* Draw the line and the intersections
		*/
		// this.__polyDrawer.drawLineSegment(context, transformFunction(lineBeingDrawn.startPoint), transformFunction(lineBeingDrawn.endPoint), false, true);

		toDrawPoints.push(pointBeingDrawn);


	}

	this.__drawLinesBetweenPoints(context, toDrawPoints, this.transformFunction);

	if(this.isDrawing) {
		$.each(intersections, function(i, intersection) {
			this.__polyDrawer.drawIntersection(context, this.transformFunction(intersection));
		}.bind(this));
	}

	/*
	* Draw all of the finished poly's
	*/

  $.each(this.currentPoly, function(i, point) {
		var pointCanvas = interactiveImage.imageToCanvasCoords(point.x, point.y);

    if(this.currentPoly.length > 1 && i != this.currentPoly.length-1) {
      var nextPoint = this.currentPoly[i+1];
			var nextPointCanvas = interactiveImage.imageToCanvasCoords(nextPoint.x, nextPoint.y);

      // this.__polyDrawer.drawLineSegment(context, pointCanvas, nextPointCanvas, "black")
    }
  }.bind(this));

	// Draw these separately, so they're always on top
  $.each(this.currentPoly, function(i, point) {
		var pointCanvas = interactiveImage.imageToCanvasCoords(point.x, point.y);

		if(i == 0) {
			this.__polyDrawer.drawFirstVertex(context, pointCanvas.x, pointCanvas.y);
		} else {
    	this.__polyDrawer.drawVertex(context, pointCanvas.x, pointCanvas.y, false, true);
		}
	}.bind(this));
}

PolyTool.prototype.__checkPointsHover = function(imageMousePos) {
	var hoveredPoint = null;
	var hoveredAnnotation = null;

	$.each(this.annotator.annotations, function(i, annotation) {
		if(annotation.type == "POLY") {
			annotation.setHoveredPoint(null);

			$.each(annotation.polygon.points, function(i, point) {
				if(point.vectorIsWithinDist(imageMousePos, this.__polyDrawer.VERTEX_OUTER_RADIUS*2)) {
					hoveredPoint = point;
					hoveredAnnotation = annotation;
				}
			}.bind(this));
		}
	}.bind(this));

	this.hoveredPoint = hoveredPoint;
	this.hoveredAnnotation = hoveredAnnotation;

	if(this.hoveredAnnotation) this.hoveredAnnotation.setHoveredPoint(this.hoveredPoint);
}

PolyTool.prototype.drawIntersections = function(context, intersections) {
	$.each(intersections, function(i, intersection) {
		this.__polyDrawer.drawIntersection(context, this.transformFunction(intersection));
	}.bind(this));
}

PolyTool.prototype.__checkIfDragAllowed = function() {
	if(this.draggedAnnotation == null || this.draggedPoint == null) return;

	this.canReleaseDrag = true;
	var polygonLines = this.draggedAnnotation.polygon.calculateLines(this.draggedAnnotation.polygon.points);

	$.each(this.draggedAnnotation.polygon.points, function(i, point) {
		if(point.x == this.draggedPoint.x && point.y == this.draggedPoint.y) {
			if(i == 0) {
				var firstLine = [point, this.draggedAnnotation.polygon.points[i+1]];
				var secondLine = [point, this.draggedAnnotation.polygon.points[this.draggedAnnotation.polygon.points.length - 1]];
			} else if(i == this.draggedAnnotation.polygon.points.length - 1) {
				var firstLine = [point, this.draggedAnnotation.polygon.points[0]];
				var secondLine = [point, this.draggedAnnotation.polygon.points[i - 1]];
			} else {
				var firstLine = [point, this.draggedAnnotation.polygon.points[i - 1]];
				var secondLine = [point, this.draggedAnnotation.polygon.points[i + 1]];
			}

			firstLine = new LineSegment(firstLine[0], firstLine[1]);
			secondLine = new LineSegment(secondLine[0], secondLine[1]);

			var intersections1 = this.__calculateIntersectionsOfLineWithLines(firstLine, polygonLines);
			var intersections2 = this.__calculateIntersectionsOfLineWithLines(secondLine, polygonLines);

			var allowedIntersections = [firstLine.startPoint, firstLine.endPoint, secondLine.startPoint, secondLine.endPoint]; // vertices can intersect
			var threshold = .1;
			intersections1 = this.__cleanPointsWithinDistance(intersections1, allowedIntersections, threshold);
			intersections2 = this.__cleanPointsWithinDistance(intersections2, allowedIntersections, threshold);

			if(intersections1.length > 0 || intersections2.length > 0) this.canReleaseDrag = false;

			// if(intersections1.length > 0) this.__polyDrawer.drawLineSegment(this.interactiveImage.context, firstLine.startPoint, firstLine.endPoint, true, false);
			// if(intersections2.length > 0) this.__polyDrawer.drawLineSegment(this.interactiveImage.context, secondLine.startPoint, secondLine.endPoint, true, false);

			this.drawIntersections(this.interactiveImage.context, intersections1);
			this.drawIntersections(this.interactiveImage.context, intersections2);
		}
	}.bind(this));
};

PolyTool.prototype.onImageMouseMove = function(imageMousePos, imageBounds) {
	this.imageMouseX = imageMousePos.x;
	this.imageMouseY = imageMousePos.y;

	if(!this.isDrawing) {
		this.__checkPointsHover(imageMousePos);
	}

	if(this.draggedPoint != null) {
		this.draggedPoint.x = this.imageMouseX;
		this.draggedPoint.y = this.imageMouseY;
	}

	$(this).trigger("change");

	if(this.draggedPoint != null) {
		this.__checkIfDragAllowed();
	}
}

PolyTool.prototype.onDocumentKeyPress = function(e) {
	if(e.keyCode == 127 || e.keyCode == 27) { // delete and esc keys (i.e. cancel)
    this.stopDraw(true);
	}
}

PolyTool.prototype.stopDraw = function(dontAddShape) {
	/*
	* New stuff
	*/
	this.__pointsBeingDrawn = [];

	//

  this.isSnapped = false;
  this.snapPoint = null;

  this.isDrawing = false;
  this.isColliding = false;
  this.lastPoint = null;

	if(!dontAddShape) {
		this.addShape(this.currentPoly);
	}
  this.currentPoly = [];

  $(this).trigger("change");
};

PolyTool.prototype.__attemptSnapPoint = function(pointToSnap, snapPoint, snapRadius) {
	if(this.__pointsBeingDrawn.length <= 2) return pointToSnap;

	if(pointToSnap.vectorIsWithinDist(snapPoint, snapRadius)) {
		return snapPoint;
	} else {
		return pointToSnap;
	}
}

function pointsWithinDist(_point1, _point2, _dist) {
	var dx = _point2.x - _point1.x;
	var dy = _point2.y - _point1.y;

	return dx*dx + dy*dy <= _dist*_dist;
}

PolyTool.prototype.onDocumentMouseUp = function(e, imagePoint) {
	if(this.canReleaseDrag) {
		if(this.draggedPoint && this.draggedAnnotation) {
			this.draggedAnnotation.setDraggedPoint(null);
		}

		this.draggedPoint = null;
		this.draggedAnnotation = null;
	}
}

PolyTool.prototype.onCanvasMouseDown = function(e, imagePoint) {
	/*
	* See if we clicked on anything and react appropriately
	*/
	if(this.draggedPoint != null) return;

	e.stopImmediatePropagation();
  if(this.isColliding) return;

	if(this.hoveredPoint != null) {
		this.draggedPoint = this.hoveredPoint;
		this.draggedAnnotation = this.hoveredAnnotation;
		this.draggedAnnotation.setDraggedPoint(this.draggedPoint);
		e.stopImmediatePropagation();
		return;
	}

  if(!this.isDrawing && !this.hoveredPoint) {
    if(this.hoverShapes.length > 0) {
      this.activeShape = this.hoverShapes[0];
      $(this).trigger("change");
      return;
    } else {
      this.isDrawing = true;
    }
  }
  this.activeShape = null;

  if(this.isSnapped) {
    imagePoint = this.snapPoint;
    this.stopDraw();
    return;
  }

	/*
	* Else start drawing
	*/
	this.__startDraw(imagePoint.x, imagePoint.y);

  this.currentPoly.push(imagePoint);
  this.lastPoint = {x: imagePoint.x, y: imagePoint.y};
	$(this).trigger("change");
};

/*
* New stuff
*/

PolyTool.prototype.__startDraw = function(imageX, imageY) {
	var point = new Vector2(imageX, imageY);
	this.__pointsBeingDrawn.push(point);
};

PolyTool.prototype.__calculateLinesFromPoints = function(points) {
	var lines = [];

	$.each(points, function(i, point) {
		// Make sure there's at least 2 points
		// Make sure we don't try a line from the last point (in which case there's no point to draw to)
		var isMoreThanOnePoint = points.length > 1;
		var isLastPoint = (i == points.length-1);

		if(isMoreThanOnePoint && !isLastPoint) {
			var nextPoint = this.__pointsBeingDrawn[i+1];
			var line = new LineSegment(point, nextPoint);
			lines.push(line);
		}
	}.bind(this));

	return lines;
};

PolyTool.prototype.__drawLinesBetweenPoints = function(context, points, pointTransformFunction) {
	var outerStrokeStyle = "rgba(20, 119, 65, 1)";
	var innerStrokeStyle = "rgba(178, 241, 130, 1)";

	var pointsTransformed = [];

	$.each(points, function(i, point) {
		if(pointTransformFunction) point = pointTransformFunction(point);
		pointsTransformed.push(point);
	});

	this.__drawer.context = context;
	this.__drawer.drawDoubleConnectedPoints(pointsTransformed, {lineWidth: 3, strokeStyle: outerStrokeStyle}, {lineWidth: 1, strokeStyle: innerStrokeStyle});
	return;
	$.each(points, function(i, point) {
		if(pointTransformFunction) point = pointTransformFunction(point);
		console.log("lorem", pointTransformFunction);

		// Make sure there's at least 2 points
		// Make sure we don't try a line from the last point (in which case there's no point to draw to)
		var isMoreThanOnePoint = points.length > 1;
		var isLastPoint = (i == points.length-1);

		if(isMoreThanOnePoint && !isLastPoint) {
			var nextPoint = this.__pointsBeingDrawn[i+1];
			if(pointTransformFunction) nextPoint = pointTransformFunction(nextPoint);

			this.__polyDrawer.drawLineSegment(context, point, nextPoint, false, true)
		}
	}.bind(this));
};

/*
* Used to calculate intersections b/w previously drawn lines and the line currently being drawn
*/

PolyTool.prototype.__calculateIntersectionsOfLineWithLines = function(mainLine, lines) {
	var intersections = [];

	$.each(lines, function(i, line) {
		// this.__polyDrawer.drawLineSegment(context, lineBeingDrawn.startPoint, lineBeingDrawn.endPoint, "black")

		var intersection = mainLine.calculateIntersectionWithLineSegment(line);
		if(intersection) intersections.push(intersection);
	}.bind(this));

	return intersections;
};

PolyTool.prototype.__cleanPointsWithinDistance = function(listOfPoints, pointsToClean, threshold) {
	var cleanedPoints = [];

	// Given a list of points, remove certain points given a second list of points
	$.each(listOfPoints, function(i, point) {
		var cleanPoint = false;
		$.each(pointsToClean, function(i, pointToClean) {
			if(point.vectorIsWithinDist(pointToClean, threshold)) {
				cleanPoint = true;
				return false;
			}
		});

		if(!cleanPoint) cleanedPoints.push(point);
	});

	return cleanedPoints;
};

export default PolyTool;
