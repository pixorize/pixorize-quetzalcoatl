import $ from 'jquery';
import Vector from '../lib/math/vector';

var Toolbox = function(image) {
  this.interactiveImage = image;
  this.canvas = this.interactiveImage.canvas;
  this.context = this.canvas.getContext("2d");

  this.activeTool = null;
  this.tools = {};

  this.hotkeys = {
    99: "c",
    67: "c",
    68: "d",
    98: "b",
    66: "b",
    114: "r",
    82: "r",
    118: "v",
    86: "v",
  }

  image.addToolbox(this);

	$('body').mouseout(this.__onMouseOut.bind(this)); // bc iframe
	$(document).on("mousemove", this.__onDocumentMouseMove.bind(this));
	$(this.interactiveImage.canvas).on("mousedown", this.__onCanvasMouseDown.bind(this));
	$(document).on("mouseup", this.__onDocumentMouseUp.bind(this));
	$(this.interactiveImage.canvas).on("dblclick", this.__onCanvasDblClick.bind(this));
  $(document).on("keydown", this.__onDocumentKeyDown.bind(this));
  $(document).on("keypress", this.__onDocumentKeyPress.bind(this));

  $(this.canvas).on("mousemove", this.__onCanvasMouseMove.bind(this));
};

Toolbox.prototype.destroy = function(context) {
	$('body').off("mouseout");
	$(document).off("mousemove");
	$(this.interactiveImage.canvas).off("mousedown");
	$(document).off("mouseup");
	$(this.interactiveImage.canvas).off("dblclick");
  $(document).off("keydown");
  $(document).off("keypress");

  $(this.canvas).off("mousemove");
};


Toolbox.prototype.draw = function(context) {
	$.each(this.tools, function(i, tool) {
    if(tool != this.activeTool) return true;
    tool.draw(context, null, this.interactiveImage);
  }.bind(this));
}

/*
*
*/

Toolbox.prototype.addTool = function(tool) {
  this.tools[tool.name] = tool;
  if(!this.activeTool) this.selectTool(tool.name); // select the first one added

  // set up toolbox ui click event
  var toolUiElem = $(this.tools[tool.name].selector);
  toolUiElem.on("click", this.toolSelectorClick.bind(this));
  toolUiElem.hover(this.showToolTip.bind(this), this.hideToolTip.bind(this));
  $(tool).on("change", this.__onToolChange.bind(this));
};

Toolbox.prototype.showToolTip = function(e) {
  var x = $(e.target).position().left;
  var y = $(e.target).position().top;

  var width = $(e.target).width();
  var height = $(e.target).height();

  var selector = "#"+e.target.id;
  var hoveredTool = null;

  $.each(this.tools, function(i, tool) {
    if(tool.selector == selector) hoveredTool = tool;
  });

  var name = hoveredTool.name;
  if(name == 'DOT') name = 'POINT';
  name = name.toLowerCase();
  //capitalize first letter
  name = name.charAt(0).toUpperCase() + name.slice(1);

  hotkey = this.hotkeys[hoveredTool.hotkey[0]];

  this.tooltip.show(x + width/2, y+height, "<b>" + name + "</b> (" + hotkey + ")");
};

Toolbox.prototype.hideToolTip = function(e) {
  this.tooltip.hide();
};

Toolbox.prototype.getTool = function(toolName) {
  return this.tools[toolName];
};

Toolbox.prototype.__onToolChange = function(e) {
  $(this).triggerHandler("change");
};

Toolbox.prototype.selectTool = function(toolName) {
  if(this.activeTool) {
    $(this.activeTool.selector).removeClass("selected");
    this.activeTool.deactivate();
  }

  this.activeTool = this.tools[toolName];
  $(this.activeTool.selector).addClass("selected");
  this.activeTool.activate();
};

Toolbox.prototype.toolSelectorClick = function(e) {
  var selector = "#"+e.target.id;
  var clickedTool = null;

  $.each(this.tools, function(i, tool) {
    if(tool.selector == selector) clickedTool = tool;
  });

  this.selectTool(clickedTool.name);
};

Toolbox.prototype.release = function() {
  // remove all click event handlers
  $.each(this.tools, function(i, tool) {
    var toolUiElem = $(this.tools[tool.name].selector);
    toolUiElem.off("click");
  }.bind(this));
};

/*
* I/O
*/

Toolbox.prototype.__onMouseOut = function(e) {
  if(e.toElement != null || e.relatedTarget != null) return; // toElement will be null if out of iframe
	$.each(this.tools, function(i, tool) {
    if(tool != this.activeTool) return true;
    tool.onMouseOut(e);
  }.bind(this));
};

// Capture non-printing characters (i.e. delete)
Toolbox.prototype.__onDocumentKeyDown = function(e) {
  this.__onDocumentKeyPress(e);
};

Toolbox.prototype.__onDocumentKeyPress = function(e) {
  if(document.activeElement != document.body) return;

	$.each(this.tools, function(i, tool) {
    if(tool.hotkey.includes(e.keyCode)) {
      this.selectTool(tool.name);
      return false;
    }

    if(tool != this.activeTool) return true;
    tool.onDocumentKeyPress(e);
  }.bind(this));
};

Toolbox.prototype.__onDocumentMouseMove = function(e) {
	var imagePoint = this.interactiveImage.pageToImageCoords(e.pageX, e.pageY);
	var bounds = this.interactiveImage.getSize();

  $.each(this.tools, function(i, tool) {
    if(tool != this.activeTool) return true;


    tool.onImageMouseMove(new Vector(imagePoint.x, imagePoint.y), bounds, this.interactiveImage);
  }.bind(this));
};

Toolbox.prototype.__onCanvasMouseDown = function(e) {
	var imagePoint = this.interactiveImage.pageToImageCoords(e.pageX, e.pageY);

  $.each(this.tools, function(i, tool) {
    if(tool != this.activeTool) return true;
    tool.onCanvasMouseDown(e, new Vector(imagePoint.x, imagePoint.y));
  }.bind(this));
};

Toolbox.prototype.__onDocumentMouseUp = function(e) {
	var imagePoint = this.interactiveImage.pageToImageCoords(e.pageX, e.pageY);

  $.each(this.tools, function(i, tool) {
    if(tool != this.activeTool) return true;
    tool.onDocumentMouseUp(new Vector(imagePoint.x, imagePoint.y));
  }.bind(this));
};

Toolbox.prototype.__onCanvasDblClick = function(e) {
	var imagePoint = this.interactiveImage.pageToImageCoords(e.pageX, e.pageY);

  $.each(this.tools, function(i, tool) {
    if(tool != this.activeTool) return true;
    tool.onCanvasDblClick(imagePoint);
  }.bind(this));
};

Toolbox.prototype.__onCanvasMouseMove = function(e) {
};

export default Toolbox;
