import $ from 'jquery';
import Vector from './vector';

var DEBUG_LINE_SEGMENT = false;
var DEBUG_DRAW = false;

function LineSegment(startPoint, endPoint) {
  this.startPoint = startPoint;
  this.endPoint = endPoint;
}

LineSegment.prototype.calculateIntersectionWithLineSegment = function(lineSegment) {
  var threshold = .1; // How much of an error is allowed to say a point is within the domain/range

  var thisAsLine = new Line(this.startPoint, this.endPoint);
  var lineSegmentAsLine = new Line(lineSegment.startPoint, lineSegment.endPoint);

  var thisSlopeIntercept = thisAsLine.calculateSlopeIntercept();
  var lineSlopeIntercept = lineSegmentAsLine.calculateSlopeIntercept();

  /*
  * START: SAME POINT SPECIAL CASE
  *   - this only happens if the startPoint and endPoint of a given line are the same, which gives 0/0
  *     -  0/0 = NaN in javascript
  *   - if the slope of either line is NaN then we have aforementioned special case
  *     - just return false (no intersection)
  */
  if(isNaN(thisSlopeIntercept.m) || isNaN(lineSlopeIntercept.m)) return false;
  /*
  * END: SAME POINT SPECIAL CASE
  */

  /*
  * ---------------------------------------------------------------------------------------------
  */


  /*
  * START: VERTICAL LINE SPECIAL CASE
  *   - slope of infinity (vertical line) special cases
  */

  // m = +/- Infinity if we have a straight line
  // x/0 => Infinity or -Infinity
  if(Math.abs(thisSlopeIntercept.m) == Infinity) {
    // The special case where "this" and "line" are both vertical lines
    // 2 vertical lines don't intersect (unless they're the same line, in which they intersect everywhere)
    //   - we don't test for them being the same line--just return false if they're both vertical
    if(Math.abs(lineSlopeIntercept.m) == Infinity) return false;

    // The special case where "this" is a vertical line and "line" is not
    var straightLineIntersection = this.__calculateIntersectionAsStraightLine(lineSegment, lineSlopeIntercept, threshold);
    if(straightLineIntersection) return straightLineIntersection;
  } else if(Math.abs(lineSlopeIntercept.m) == Infinity) {
    // The special case where "line" is a straight line and "this" is not
    var straightLineIntersection = lineSegment.__calculateIntersectionAsStraightLine(this, thisSlopeIntercept, threshold);
    if(straightLineIntersection) return straightLineIntersection;
  }

  /*
  * END: VERTICAL LINE SPECIAL CASE
  */

  /*
  * ---------------------------------------------------------------------------------------------
  */

  // If two lines intersect, this is the solution to their intersection
  var intersectionX = (lineSlopeIntercept.b - thisSlopeIntercept.b) / (thisSlopeIntercept.m - lineSlopeIntercept.m);
  var intersectionY = thisSlopeIntercept.m * intersectionX + thisSlopeIntercept.b;
  var intersectionPoint = new Vector(intersectionX, intersectionY);

  // Only have to check domain and range--we already know the point is on the lines
  // bc of the way it was calculated
  var isIntersecting = this.pointInDomainAndRange(intersectionPoint, threshold) && lineSegment.pointInDomainAndRange(intersectionPoint, threshold);

  if(DEBUG_DRAW && DEBUG_LINE_SEGMENT) {
    /*
    * Transform everything to canvas coords
    */
    var thisAsLineTransformed = new Line(DEBUG_TRANSFORM_FUNCTION(this.startPoint), DEBUG_TRANSFORM_FUNCTION(this.endPoint));
    var thisAsLineTransformedAndExtended = thisAsLineTransformed.extendPoints(999999);

    var lineSegmentAsLineTransformed = new Line(DEBUG_TRANSFORM_FUNCTION(lineSegmentAsLine.startPoint), DEBUG_TRANSFORM_FUNCTION(lineSegmentAsLine.endPoint));
    var lineSegmentAsLineTransformedAndExtended = lineSegmentAsLineTransformed.extendPoints(999999);

    var intersectionPointTransformed = DEBUG_TRANSFORM_FUNCTION(intersectionPoint);

    /*
    * And actually draw w/ the transformations applied
    */
    // Draw this line
    DEBUG_DRAWER.drawLineSegment(thisAsLineTransformedAndExtended.startPoint, thisAsLineTransformedAndExtended.endPoint, {strokeStyle: "blue"}); // draw the line
    DEBUG_DRAWER.drawLineSegment(thisAsLineTransformed.startPoint, thisAsLineTransformed.endPoint, {strokeStyle: "cyan"}); // highlight the segment of interest

    // Draw the line we're checking against
    DEBUG_DRAWER.drawLineSegment(lineSegmentAsLineTransformedAndExtended.startPoint, lineSegmentAsLineTransformedAndExtended.endPoint, {strokeStyle: "DarkGoldenRod"});
    DEBUG_DRAWER.drawLineSegment(lineSegmentAsLineTransformed.startPoint, lineSegmentAsLineTransformed.endPoint, {strokeStyle: "yellow"});

    if(isIntersecting) {
      // Draw an "X" at the intersection
      DEBUG_DRAWER.drawX(intersectionPointTransformed.x, intersectionPointTransformed.y, 25, 25, {strokeStyle: "red"});
    }
  }

  return isIntersecting ? intersectionPoint : false;
};

LineSegment.prototype.__calculateIntersectionAsStraightLine = function(lineSegment, lineSegmentSlopeIntercept, threshold) {
  // Since we are a straight line, make sure the inetrsection exists in the otehr line's domain
  if(lineSegment.isInDomain(this.startPoint.x, threshold)) {
    var intersection = new Vector(this.startPoint.x, lineSegmentSlopeIntercept.m * this.startPoint.x + lineSegmentSlopeIntercept.b);

    // And the intersection must lie in both segments' ranges
    if(this.isInRange(intersection.y, threshold) && lineSegment.isInRange(intersection.y, threshold)) {
      return intersection;
    }
  }

  return false;
}

LineSegment.prototype.calculateIntersectionWithPoint = function(point) {
  var thisAsLine = new Line(this.startPoint, this.endPoint);
  var thisSlopeIntercept = thisAsLine.calculateSlopeIntercept();

  var intersectionX = point.x; // The only point they intersect, if they can
  var intersectionY = thisSlopeIntercept.m * intersectionX + thisSlopeIntercept.b;
  var intersectionPoint = new Vector(intersectionX, intersectionY);

  var threshold = .2; // How much of an error is allowed to say a point is within the domain/range
  var onLineSegment = this.pointInDomainAndRange(intersectionPoint, threshold);
  var isIntersecting = onLineSegment && intersectionPoint.vectorIsWithinDist(point, threshold); // Intersection is on the line and equal to the point (w/in threshold)

  if((DEBUG_DRAW && DEBUG_LINE_SEGMENT)) {
    var thisAsLineTransformed = new Line(DEBUG_TRANSFORM_FUNCTION(this.startPoint), DEBUG_TRANSFORM_FUNCTION(this.endPoint));
    var thisAsLineTransformedAndExtended = thisAsLineTransformed.extendPoints(999999);
    var intersectionPointTransformed = DEBUG_TRANSFORM_FUNCTION(intersectionPoint);

    DEBUG_DRAWER.drawLineSegment(thisAsLineTransformedAndExtended.startPoint, thisAsLineTransformedAndExtended.endPoint, {strokeStyle: "blue"}); // draw the line
    DEBUG_DRAWER.drawLineSegment(thisAsLineTransformed.startPoint, thisAsLineTransformed.endPoint, {strokeStyle: "cyan"}); // highlight the segment of interest

    if(isIntersecting) {
      DEBUG_DRAWER.drawX(intersectionPointTransformed.x, intersectionPointTransformed.y, 25, 25, {strokeStyle: "red"});
    }
  }

  return isIntersecting ? intersectionPoint : false;
};

LineSegment.prototype.pointInDomainAndRange = function(point, threshold) {
  if(threshold == undefined) threshold = 0;

  var pointIsInDomain = this.isInDomain(point.x, threshold);
  var pointIsInRange = this.isInRange(point.y, threshold);

  return pointIsInDomain && pointIsInRange;
};

LineSegment.prototype.isInDomain = function(x, threshold) {
  if(threshold == undefined) threshold = 0;

  var minX = Math.min(this.startPoint.x, this.endPoint.x);
  var maxX = Math.max(this.startPoint.x, this.endPoint.x);
  return (x >= minX - threshold) && (x <= maxX + threshold);
};

LineSegment.prototype.isInRange = function(y, threshold) {
  if(threshold == undefined) threshold = 0;

  var minY = Math.min(this.startPoint.y, this.endPoint.y);
  var maxY = Math.max(this.startPoint.y, this.endPoint.y);
  return (y >= minY - threshold) && (y <= maxY + threshold);
};

LineSegment.prototype.domain = function() {
  var minX = Math.min(this.startPoint.x, this.endPoint.x);
  var maxX = Math.max(this.startPoint.x, this.endPoint.x);
  return new Vector(minX, maxX);
};

LineSegment.prototype.range = function() {
  var minY = Math.min(this.startPoint.y, this.endPoint.y);
  var maxY = Math.max(this.startPoint.y, this.endPoint.y);
  return new Vector(minY, maxY);
};

export default LineSegment;
