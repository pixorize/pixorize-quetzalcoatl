import $ from 'jquery';

var Vector = function(x, y) {
	this.x = x;
	this.y = y;
};

Vector.prototype.add = function(vector) {
	return new Vector(this.x + vector.x, this.y + vector.y);
};

Vector.prototype.subtract = function(vector) {
	return new Vector(this.x - vector.x, this.y - vector.y);
};

Vector.prototype.vectorWithinDist = function(vector, dist) {
	var distSquared = this.distanceSquared(vector);
	return distSquared <= dist*dist;
};

Vector.prototype.vectorsWithinDist = function(vectors, dist) {
	var withinDist = false;

	$.each(vectors, function(i, vector) {
		if(this.vectorWithinDist(vector, dist)) withinDist = true;
		return false;
	}.bind(this));

	return withinDist;
};

Vector.prototype.distanceSquared = function(vector) {
	var dy = vector.y - this.y;
	var dx = vector.x - this.x;
	return dy*dy + dx*dx;
};

Vector.prototype.withinRect = function(rect) {
	var contained = false;

	var xMin = rect.x;
	var xMax = rect.x + rect.width;
	var yMin = rect.y;
	var yMax = rect.y + rect.height;

	if(this.x >= xMin && this.x <= xMax) {
		if(this.y >= yMin && this.y <= yMax) {
			contained = true;
		}
	}

	return contained;
};

Vector.prototype.magnitude = function() {
	return Math.sqrt(this.x*this.x + this.y*this.y);
};










//function Vector(x, y) {
	//this.x = x;
	//this.y = y;
//}

/*
* Basic operations
*/

//Vector.prototype.add = function(vector) {
	//return new Vector(this.x + vector.x, this.y + vector.y);
//};

//Vector.prototype.subtract = function(vector) {
	//return new Vector(this.x - vector.x, this.y - vector.y);
//};

//Vector.prototype.multiply = function(constant) {
	//return new Vector(this.x * constant, this.y * constant);
//};

//Vector.prototype.magnitude = function() {
	//return Math.sqrt(this.x*this.x + this.y*this.y);
//};

//Vector.prototype.normalize = function() {
	//return this.multiply(1/this.magnitude());
//};

/*
* Collision detection
*/

//Vector.prototype.distance = function(vector) {
	//var dy = vector.y - this.y;
	//var dx = vector.x - this.x;
	//return Math.sqrt(dy*dy + dx*dx);
//};

//Vector.prototype.distanceSquared = function(vector) {
	//var dy = vector.y - this.y;
	//var dx = vector.x - this.x;
	//return dy*dy + dx*dx;
//};

Vector.prototype.vectorIsWithinDist = function(vector, dist) {
  var distSquared = this.distanceSquared(vector);
  return distSquared <= dist*dist;
};

export default Vector;
