import $ from 'jquery';

var DEBUG_RAY = false;
var DEBUG_DRAW = false;

var Ray = function(startPoint, endPoint) {
  this.startPoint = startPoint;
  this.endPoint = endPoint;
};

Ray.prototype.extendPoint = function(scale) {
  // Extend the endPoint by scale (i.e. ray goes from startPoint to ~Infinity)
  var directionVector = this.endPoint.subtract(this.startPoint).normalize();
  var moveVector = directionVector.multiply(scale);

  return new Ray(this.startPoint, this.startPoint.add(moveVector))
};

Ray.prototype.calculateIntersectionWithLineSegment = function(lineSegment) {
  // Approximate this as a really long line segment in one direction
  var thisExtended = this.extendPoint(999999);
  var thisAsLineSegment = new LineSegment(thisExtended.startPoint, thisExtended.endPoint);

  // Annnd the intersection logic is already there
  return thisAsLineSegment.calculateIntersectionWithLineSegment(lineSegment);;
};

Ray.prototype.calculateIntersectionWithLineSegment = function(lineSegment) {
  // Approximate this as a really long line segment in one direction
  var thisExtended = this.extendPoint(999999);
  var thisAsLineSegment = new LineSegment(thisExtended.startPoint, thisExtended.endPoint);

  // Annnd the intersection logic is already there
  return thisAsLineSegment.calculateIntersectionWithLineSegment(lineSegment);;
};

Ray.prototype.calculateIntersectionWithPoint = function(point) {
  // Approximate this as a really long line segment in one direction
  var thisExtended = this.extendPoint(999999);
  var thisAsLineSegment = new LineSegment(thisExtended.startPoint, thisExtended.endPoint);

  // Annnd the intersection logic is already there
  return thisAsLineSegment.calculateIntersectionWithPoint(point);
};

Ray.prototype.calculateIntersectionWithPoints = function(points) {
  var intersections = [];

  $.each(points, function(i, point) {
    var intersection = this.calculateIntersectionWithPoint(point);
    if(intersection) intersections.push(intersection);
  }.bind(this));

  return intersections;
};

export default Ray;
