import $ from 'jquery';

function Line(startPoint, endPoint) {
  this.startPoint = startPoint;
  this.endPoint = endPoint;
}

Line.prototype.extendPoints = function(scale) {
  // Extend the points in both directions by scale
  var directionVector = this.endPoint.subtract(this.startPoint).normalize();
  var moveVector = directionVector.multiply(scale);
  return new Line(this.startPoint.subtract(moveVector), this.startPoint.add(moveVector))
};

Line.prototype.calculateSlopeIntercept = function() {
  var m = (this.endPoint.y-this.startPoint.y)/(this.endPoint.x-this.startPoint.x);
  var b = this.endPoint.y - m*this.endPoint.x;

  return {m: m, b: b};
};

export default Line;
