import $ from 'jquery';

import Vector from './vector';
import LineSegment from './line-segment';

var DEBUG_POLYGON = false;
var DEBUG_DRAW = false;

var Polygon = function(points) {
  this.points = points;
  this.centerPoint = this.calculateCenterPoint(this.points);
  this.lines = this.calculateLines(this.points);
};

/*
* There's still a problem here if the ray goes exactly through a vertex
*/
Polygon.prototype.doesContainsPoint = function(point) {
  this.lines = this.calculateLines(this.points);

  // Make sure we aren't actually mouse over a vertex (bc weird behavior--while loop below will be infinite)
  if(point.vectorsWithinDist(this.points)) return false;

  // raycast a random ray from the point in question
  var ray;
  var rayIntersectionsWithVertices;
  var rayIntersectsAVertex;
  var i = 0;
  do {
    i++;
    ray = new Ray(point, new Vector(point.x + Math.random()*10+1, point.y + Math.random()*10+1));
    // ray = new Ray(point, new Vector(point.x+1, point.y));
    rayIntersectionsWithVertices = ray.calculateIntersectionWithPoints(this.points);
    rayIntersectsAVertex = rayIntersectionsWithVertices.length > 0;
  } while (rayIntersectsAVertex && (i < 15)); // Make sure the ray doesn't go through a vertex bc weird math
  // Also added "i < 15" so we don't get caught in an infinite loop--would be better to be a bit buggy than to crash the user's browser

  // var ray = new Ray(new Vector(10, 100), new Vector(point.x + 999999999, point.y));

  // Check intersection of ray with each line segment in polygon
  var intersections = [];
  $.each(this.lines, function(i, line) {
    var intersection = ray.calculateIntersectionWithLineSegment(line);
    if(intersection) intersections.push(intersection);
  });

  // Even # of intersections = inside shape
  // Odd # of intersections = outside shape
  var numIntersections = intersections.length;
  var isEven = (numIntersections % 2) == 0;
  var doesContainPoint = !Boolean(isEven);

  if(DEBUG_DRAW && DEBUG_POLYGON) {
    var rayTransformed = new Ray(DEBUG_TRANSFORM_FUNCTION(ray.startPoint), DEBUG_TRANSFORM_FUNCTION(ray.endPoint));
    DEBUG_DRAWER.drawLineSegment(rayTransformed.startPoint, rayTransformed.extendPoint(999999).endPoint, {strokeStyle: "green"});

    // Draw all the lines of this polygon
    $.each(this.lines, function(i, line) {
      var lineTransformed = new LineSegment(DEBUG_TRANSFORM_FUNCTION(line.startPoint), DEBUG_TRANSFORM_FUNCTION(line.endPoint));
      DEBUG_DRAWER.drawLineSegment(lineTransformed.startPoint, lineTransformed.endPoint, {strokeStyle: "yellow"});
    });

    // Draw all the intersections b/w the polygon and the ray
    $.each(intersections, function(i, intersection) {
      var intersectionTransformed = DEBUG_TRANSFORM_FUNCTION(intersection);
      DEBUG_DRAWER.drawX(intersectionTransformed.x, intersectionTransformed.y, 25, 25, {strokeStyle: "red"});
    });
  }

  // if(doesContainPoint) {
  //   console.log(intersections);
  // }

  return doesContainPoint;
};

Polygon.prototype.calculateCenterPoint = function(points) {
  var centerX = 0;
  var centerY = 0;

  $.each(points, function(i, point) {
    centerX += point.x;
    centerY += point.y;
  });

  var numPoints = points.length;
  centerX /= numPoints;
  centerY /= numPoints;

  var centerPoint = new Vector(centerX, centerY);

  if(DEBUG_DRAW && DEBUG_POLYGON) {
    var centerPointTransformed = DEBUG_TRANSFORM_FUNCTION(centerPoint);
    DEBUG_DRAWER.drawCircle(centerPointTransformed.x, centerPointTransformed.y, 10, {fillStyle: "green"});
  }

  return centerPoint;
};

Polygon.prototype.calculateLines = function(points) {
  var numPoints = points.length;
  var lines = [];

  $.each(points, function(i, point) {
    if((numPoints > 1) && (i != numPoints-1)) {
      var nextPoint = points[i+1];
      var line = new LineSegment(point, nextPoint);
      lines.push(line);
    }
  }.bind(this));

  // connect from the last point to the first point
  var line = new LineSegment(points[numPoints-1], points[0]);
  lines.push(line);

  return lines;
};

export default Polygon;
