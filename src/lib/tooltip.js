import $ from 'jquery';
import 'jquery.waitforimages';

var Tooltip = function() {
  this.elem = this.createElem('tooltip', 'body');
  this.hide(); /* hide by default */
  this.setInitialStyles();
}

Tooltip.prototype.setInitialStyles = function() {
  // positioning
  this.elem.css('position', 'fixed');
  
  // text
  this.TEXT_COLOR = '#FFF';
  this.TEXT_PADDING = 10 + 'px';

  this.elem.css('color', this.TEXT_COLOR);
  this.elem.css('padding', this.TEXT_PADDING);

  // box style
  this.BACKGROUND_COLOR = 'rgba(17, 17, 17, .9)';
  this.BORDER_RADIUS = '2px';
  this.BOX_SHADOW = '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)';

  this.elem.css('background-color', this.BACKGROUND_COLOR);
  this.elem.css('border-radius', this.BORDER_RADIUS);
  this.elem.css('box-shadow', this.BOX_SHADOW);

  // box width
  this.MAX_WIDTH_PERCENT = '50vw';
  this.MAX_WIDTH_PX = '500px';

  $('#tooltip-text-wrapper').css('max-width', this.MAX_WIDTH_PERCENT);
  $('#tooltip-text').css('max-width', this.MAX_WIDTH_PX);

  // arrow
  this.TRIANGLE_WIDTH = '10px'; /* for the arrow */
  this.makeDownArrow();

  // misc
  // perspective(1px) fixes text being on a half pixel in webkit
  this.elem.css({
    position: 'fixed',
    transform: 'perspective(1px) translateX(-50%)'
  });

  $('#tooltip-arrow').css({
    position: 'fixed',
    left: '50%',
    transform: 'perspective(1px) translateX(-50%)',
    top: '-' + this.TRIANGLE_WIDTH
  });

  this.elem.css('z-index', '1000002'); // bring to top
};

Tooltip.prototype.createElem = function(tooltipSelector, parentSelector) {
  // Only one tooltip at a time
  if(document.getElementById(tooltipSelector)) {
    return $('#'+tooltipSelector);
  }

  var html = '';

  html +=  '<div id="' + tooltipSelector + '">';
  html +=  '  <div id="tooltip-arrow"></div>';
  html +=  '  <div id="tooltip-text-wrapper">';
  html +=  '    <div id="tooltip-text"></div>';
  html +=  '  </div>';
  html +=  '</div>';

  this.arrowElem = $('#tooltip-arrow');

  $(parentSelector).append(html);
  return $('#' + tooltipSelector);
};

Tooltip.prototype.show = function(x, y, text) {
  if(this.isShowing && x == this.x && y == this.y) return;

  this.isShowing = true;

  this.x = x;
  this.y = y;

  var triangleWidth = parseInt(this.TRIANGLE_WIDTH, 10); // remove 'px' units

  this.elem.css({
    left: x + 'px',
    top: (y + triangleWidth) + 'px'
  });

  // Make some sizing corrections after rendering
  this.resetBounds();

  $('#tooltip-text').html(text).promise().done(function() {
    $('#tooltip-text').waitForImages(function() {
      this.elem.show().promise().done(function() {
        // Should be after all rendering is finished
        this.keepInBounds(x, y);
        this.elem.css('opacity', 1);
      }.bind(this));
    }.bind(this));
  }.bind(this));


  // Flip upside down if necessary
  //$('#tooltip-text').html(text).promise().done(function() {
      ////// This is because now that we have images, they sometimes take time to load and this is not immediately picked up by the .promise().done() bit
      //setTimeout(function() {
    //this.elem.show().promise().done(function() {
      ////this.loremIpsum(x, y, text);
      //}.bind(this), 1);
    //}.bind(this));
  //}.bind(this));
}

Tooltip.prototype.resetBounds = function() {
  $('#tooltip-arrow').css('left', '50%');
};

Tooltip.prototype.keepInBounds = function(x, y) {
  var offset = this.elem.offset();
  var left = offset.left;
  var right = $('#tooltip')[0].getBoundingClientRect().right;
  right = offset.left + this.elem.outerWidth();

  var top = offset.top;
  var bottom = $('#tooltip')[0].getBoundingClientRect().bottom;
  bottom = offset.top + this.elem.outerHeight();

  var maxLeft = $(window).outerWidth();
  var maxTop = $(window).outerHeight();

  var currLeft = parseInt(this.elem.css('left'), 10);
  var currRight = parseInt(this.elem.css('right'), 10);
  var currTop = parseInt(this.elem.css('top'), 10);
  var currBottom = parseInt(this.elem.css('bottom'), 10);

  if(left < 0) {
    // Going off the screen to the left
    
    this.elem.css('left', currLeft - left);
    $('#tooltip-arrow').css('left', x);
  } else if(currRight < 0) {
    // Going off the screen to the right

    var shiftAmt = right - maxLeft; // How much we are going over by
    this.elem.css('left', currLeft - shiftAmt);
    $('#tooltip-arrow').css('left', (x - left) + shiftAmt);
  }

  var tooltipHeight = this.elem.outerHeight();

  if(currBottom < 0) {
    const arrowHeight = 10;
    var dotHeight = 30;
    this.makeUpArrow();
    this.elem.css('top', currTop - tooltipHeight - arrowHeight - dotHeight + 2);
    let fudge = 1;
    let padding = 1;

    $('#tooltip-arrow').css({
      top: tooltipHeight - 1,
    });
  } else {
    this.makeDownArrow();

    $('#tooltip-arrow').css({
      top: -10,
    });
  }
};

Tooltip.prototype.loremIpsum = function(x, y, text) {
  var padding = 10;
  var dir = 'top';

  $('#tooltip-text').css({
    'white-space': 'nowrap'
  });

  var textWidth = $('#tooltip-text').outerWidth() + this.TEXT_PADDING*2;
  if(textWidth > 500) textWidth = 500;

  $('#tooltip-text').css({
    'white-space': 'normal'
  });

  var leftEdgeTooltip = x - textWidth/2;
  var rightEdgeTooltip = x + textWidth/2;
  var rightEdgeOfScreen = $(window).scrollLeft() + $(window).width();

  var tooltipHeight = this.elem.outerHeight();
  var bottomEdgeTooltip = y+padding+ + tooltipHeight;
  var bottomEdgeOfScreen = $(window).scrollTop() + $(window).height();

  this.makeDownArrow();
  var tooltipY = y;
  var fudge = 1;

  if(bottomEdgeTooltip > bottomEdgeOfScreen) {
    this.makeUpArrow();
    y -= tooltipHeight;
    tooltipY -= 3*padding;
    padding *= -3;
    fudge *= -1;
  } else {
    this.makeDownArrow();
  }

  var left;

  if(leftEdgeTooltip < 0 && rightEdgeTooltip > rightEdgeOfScreen) {
    // Browser should handle this on its own
  } else if(leftEdgeTooltip < 0) {
    left = x - leftEdgeTooltip;
  } else if(rightEdgeTooltip > rightEdgeOfScreen) {
    left = x - (rightEdgeTooltip - rightEdgeOfScreen);
  } else {
    left = x;
  }

  $('#tooltip-arrow').css({
    position: 'fixed',
    left: x,
    top: tooltipY+fudge, // the +1 is bc if we land on a half-pixel it will have a gap
    transform: 'perspective(1px) translate(-50%, 0)'
  });

  this.elem.css({
    position: 'fixed',
    left: left,
    top: y+padding,
    width: (textWidth - this.TEXT_PADDING*2) + 'px'
  });

}

Tooltip.prototype.hide = function() {
  this.isShowing = false;
  this.elem.hide();
}

/*
 * Arrows
 */

Tooltip.prototype.makeUpArrow = function(x, y, text) {
  this.__makeArrowCommon();

  $('#tooltip-arrow').css('border-top', this.TRIANGLE_WIDTH + ' solid ' + this.BACKGROUND_COLOR);
  $('#tooltip-arrow').css('border-bottom', '');
};

Tooltip.prototype.makeDownArrow = function(x, y, text) {
  this.__makeArrowCommon();

  $('#tooltip-arrow').css('border-bottom', this.TRIANGLE_WIDTH + ' solid ' + this.BACKGROUND_COLOR);
  $('#tooltip-arrow').css('border-top', '');
};

Tooltip.prototype.__makeArrowCommon = function(x, y, text) {
  $('#tooltip-arrow').css('width', '0');
  $('#tooltip-arrow').css('height', '0');
  $('#tooltip-arrow').css('border-left', this.TRIANGLE_WIDTH + ' solid transparent');
  $('#tooltip-arrow').css('border-right', this.TRIANGLE_WIDTH + ' solid transparent');
};

export default Tooltip;
