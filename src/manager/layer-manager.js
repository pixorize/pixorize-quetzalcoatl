const LayerManager = function() {};

LayerManager.prototype.sort = (annotations) => {
  try {
    const order = ['POLY', 'CIRCLE', 'DOT']; // order is reversed here

    const compare = (a, b) => {
      // Return a number to determine the sort order b/w "a" and "b"
      // Return > 0 if a > b
      // Return = 0 if a = b
      // Return < 0 if a < b
      const aIndex = order.indexOf(a.type);
      const bIndex = order.indexOf(b.type);

      if (aIndex === bIndex) return 0;
      if (aIndex > bIndex) return 1;
      if (aIndex < bIndex) return -1;

      return null;
    };

    annotations.sort(compare);
  } catch (e) {}
};

export default LayerManager;
