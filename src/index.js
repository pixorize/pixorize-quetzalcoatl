import $ from 'jquery';

const DEBUG = false;

const InteractiveImageCanvas = function constructor() {
  // this.canvasContainer = document.createElement("div");
  // this.canvasContainer.setAttribute('id', 'canvas-container');
  this.canvas = document.createElement('canvas');
  this.canvas.ontouchmove = (e) => e.preventDefault();

  // $(this.canvasContainer).append(this.canvas);
  this.context = this.canvas.getContext('2d');
  this.__isDirty = true;
  setInterval(this.__step.bind(this), 25);

  // Disable right click
  $(this.canvas).bind('contextmenu', (e) => false);
};

InteractiveImageCanvas.prototype.__step = function __step(scale) {
  // This means we can only draw at a max of 60fps
  if (this.__isDirty) this.draw();
};

InteractiveImageCanvas.prototype.serialize = function serialize(scale) {
  return this.annotator.serialize(this.zoomer.zoom);
};

InteractiveImageCanvas.prototype.onModuleChange = function () {
  this.__isDirty = true;
};

InteractiveImageCanvas.prototype.draw = function () {
  if (!this.img) {
    throw 'InteractiveImageCanvas.draw(): No img set. Set using InteractiveImageCanvas.setImg(img)';
  }

  let zoom = 1;
  if (this.zoomer) zoom = this.zoomer.zoom;

  const scroll = this.getScroll();

  if (!DEBUG) this.context.clearRect(0, 0, this.img.width, this.img.height);
  // Math.floor everything as performance increase. Landing on pixels --> calculation of anti-aliasing
  // this.context.drawImage(this.img, Math.floor(scroll.x/zoom), Math.floor(scroll.y/zoom), Math.floor(this.img.width), Math.floor(this.img.height), 0, 0, Math.floor(this.img.width*zoom), Math.floor(this.img.height*zoom));
  // This is complicated bc iOS freaks out if your source image is outside "this.img"'s boundaries
  const scrollX = Math.floor(scroll.x / zoom);
  const scrollY = Math.floor(scroll.y / zoom);
  this.context.drawImage(this.img, scrollX, scrollY, Math.floor(this.img.width - scrollX), Math.floor(this.img.height - scrollY), 0, 0, Math.floor(this.img.width * zoom - scrollX * zoom), Math.floor(this.img.height * zoom - scrollY * zoom));

  if (this.annotator) this.annotator.draw(this.context, this.scroller);
  if (this.toolbox) this.toolbox.draw(this.context, this.scroller);

  this.__isDirty = false;
};

InteractiveImageCanvas.prototype.getScroll = function () {
  let scrollX = 0;
  let scrollY = 0;

  if (this.scroller) {
    scrollX = this.scroller.scrollX;
    scrollY = this.scroller.scrollY;
  }

  return { x: scrollX, y: scrollY };
};

InteractiveImageCanvas.prototype.getSize = function () {
  const size = { width: this.img.width * this.zoomer.zoom, height: this.img.height * this.zoomer.zoom };
  return size;
};

InteractiveImageCanvas.prototype.setSize = function (width, height) {
  this.width = width;
  this.height = height;

  $(this.canvas)[0].setAttribute('width', this.width);
  $(this.canvas)[0].setAttribute('height', this.height);

  if (this.img && this.scroller) {
    let zoom = 1;
    if (this.zoomer) zoom = this.zoomer.zoom;
    this.scroller.setBounds(this.img.width * zoom - this.width, this.img.height * zoom - this.height);
  }

  this.__isDirty = true;
  $(this).triggerHandler('change');
};

InteractiveImageCanvas.prototype.getBounds = function () {
  // var left = parseInt($(this.canvas).css("left"), 10);
  // var top = parseInt($(this.canvas).css("top"), 10);

  const offset = $(this.canvas).offset();
  const { left } = offset;
  const { top } = offset;

  const bounds = {
    x: left,
    y: top,
    width: this.width,
    height: this.height,
  };
  return bounds;
};

InteractiveImageCanvas.prototype.setImg = function (img) {
  this.img = img;
  if (this.scroller) {
    this.scroller.setBounds(this.img.width - this.width, this.img.height - this.height);
  }
  this.draw();
  if (this.positioner) this.positioner.position();
};

InteractiveImageCanvas.prototype.addScroller = function (scroller) {
  this.scroller = scroller;
  $(this.scroller).change(this.onModuleChange.bind(this));
};

InteractiveImageCanvas.prototype.addUI = function (ui) {
  this.ui = ui;
};

/*
 * Behaviors
 */

InteractiveImageCanvas.prototype.addAnnotator = function (annotator) {
  this.annotator = annotator;
  const parent = this;
  $(this.annotator).change(() => {
    parent.onModuleChange();
  });
  if (this.zoomer) this.zoomer.setOrigAnnotations(this.annotator.annotations);
  if (this.zoomer) this.zoomer.updateAnnotator(this.zoomer.zoom, 1);
  this.draw();
};

InteractiveImageCanvas.prototype.addPincherPanner = function (pincherPanner) {
  this.pincherPanner = pincherPanner;
  if (this.width) this.draw();
};

InteractiveImageCanvas.prototype.addZoomer = function (zoomer) {
  this.zoomer = zoomer;
  $(this.zoomer).change(this.onModuleChange.bind(this));
  if (this.width) this.draw();
  if (this.positioner) this.positioner.position();
};

InteractiveImageCanvas.prototype.addPositioner = function (positioner) {
  this.positioner = positioner;
  this.positioner.position();
  $(this).triggerHandler('change');
};

InteractiveImageCanvas.prototype.addToolbox = function (toolbox) {
  this.toolbox = toolbox;
  $(this).triggerHandler('change');

  $(this.toolbox).on('change', this.__onToolboxChange.bind(this));
};

InteractiveImageCanvas.prototype.__onToolboxChange = function (toolbox) {
  this.onModuleChange();
};

InteractiveImageCanvas.prototype.destroy = function () {
  this.canvas.remove();
  if (this.ui) this.ui.destroy();
  if (this.pincherPanner) this.pincherPanner.destroy();
  if (this.annotator) this.annotator.destroy();

  if (this.toolbox) {
    $(this.toolbox).off('change');
    this.toolbox.destroy();
  }
};

InteractiveImageCanvas.prototype.pageToImageCoords = function (documentMouseX, documentMouseY) {
  const offset = $(this.canvas).offset(); // This is why we had to tightly couple

  const scroll = this.getScroll();
  const relImageMouseX = scroll.x + documentMouseX - offset.left;
  const relImageMouseY = scroll.y + documentMouseY - offset.top;

  return { x: relImageMouseX, y: relImageMouseY };
};

InteractiveImageCanvas.prototype.imageToPageCoords = function (imageX, imageY) {
  const offset = $(this.canvas).offset(); // This is why we had to tightly couple
  const scroll = this.getScroll();

  const relMouseX = offset.left + imageX - scroll.x;
  const relMouseY = offset.top + imageY - scroll.y;

  return { x: relMouseX, y: relMouseY };
};

InteractiveImageCanvas.prototype.imageToCanvasCoords = function (imageX, imageY) {
  const scroll = this.getScroll();
  const relCanvasMouseX = imageX - scroll.x;
  const relCanvasMouseY = imageY - scroll.y;

  return { x: relCanvasMouseX, y: relCanvasMouseY };
};

export default InteractiveImageCanvas;
