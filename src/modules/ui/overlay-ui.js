import $ from 'jquery';
import Vector from '../../lib/math/vector';

import logoWithTextImage from '../../assets/logo_with_text_viewer.svg';
import zoomInImage from '../../assets/zoom_in_icon.svg';
import zoomOutImage from '../../assets/zoom_out_icon.svg';
import lightOnImage from '../../assets/light_on.svg';
import lightOffImage from '../../assets/light_off.svg';

var DEBUG = false;

var OverlayUI = function(interactiveImage) {
   //DEBUG = true;
  //this.DEBUG = true;

	this.interactiveImage = interactiveImage;
	this.isMobile = /Mobi|Tablet|iPad|iPhone/.test(navigator.userAgent);

	/*
	 * Constants
	 */

	this.LOGO_SRC = logoWithTextImage;
	this.ZOOM_IN_SRC = zoomInImage;
	this.ZOOM_OUT_SRC = zoomOutImage;
	this.LIGHT_ON_SRC = lightOnImage;
	this.LIGHT_OFF_SRC = lightOffImage;

  this.preloadImage(this.LIGHT_OFF_SRC);

	this.LOGO_ICON_WIDTH = 120;
	this.HEIGHT = 45;
	this.PADDING = 10;

	/*
	 * Setup
	 */
	this.__createUIContainer();
	this.__createItems();
	this.__setupHandlers();

	this.hide(true);
	this.__onInteractiveImageChange();

	$(window).resize(function() {
    this.__onInteractiveImageChange();
	}.bind(this));
};

OverlayUI.prototype.destroy = function() {
	this.__destroyHandlers();
	this.container.remove();
};

OverlayUI.prototype.__createUIContainer = function() {
	var containerElem = '<div id="viewer_ui_container"></div>';
	$(".ii").append(containerElem);
  $(".ii").css("position", "relative");

	this.container = $("#viewer_ui_container");
	this.container.css("height", (this.HEIGHT + this.PADDING) + "px");
	if(!DEBUG) {
    this.container.css("overflow", "hidden");
	} else {
		this.container.css("box-sizing", "border-box");
		this.container.css("border", "solid 1px red");
	}
  if(!this.DEBUG) {
    this.container.css("opacity", "0");
    var parent = this;
    setTimeout(function() {
      parent.container.css("transition", "opacity 1s");
    }, 1);
  }
	this.container.css("background", "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, .65))");
	this.container.css("pointer-events", "none");
};

OverlayUI.prototype.__createItems = function() {
  var fullscreenWidth = 20;
  fullscreenWidth = - this.PADDING;
	var zoomIconWidth = 20;
	var lightWidth = 32;
	var logoIconWidth = this.LOGO_ICON_WIDTH;

	this.__items = [];
	this.__addItem("viewer_logo_img", this.LOGO_SRC, this.PADDING, this.__logoClick);
	this.__addItem("light_img", this.LIGHT_ON_SRC, -2*this.PADDING - lightWidth - fullscreenWidth, this.__lightClick);
	this.__addItem("zoom_in_img", this.ZOOM_IN_SRC, -3*this.PADDING - zoomIconWidth - lightWidth - fullscreenWidth, this.__zoomInClick);
	this.__addItem("zoom_out_img", this.ZOOM_OUT_SRC, -4*this.PADDING - 2*zoomIconWidth - lightWidth - fullscreenWidth, this.__zoomOutClick);
	this.__positionItems();
};

OverlayUI.prototype.__positionItems = function() {
	if(!this.bounds) return;

	$.each(this.__items, function(i, item) {
		item.elem.css("position", "absolute");
		item.elem.css("left", item.xPos + "px");

		if(item.xPos < 0) {
			var widthOfImage = this.bounds.width;
			var widthOfWindow = $(window).width();
			var rightEdge = Math.min(widthOfImage, widthOfWindow);

			var xPos = rightEdge + item.xPos;
			item.elem.css("left", xPos + "px");
		}
	}.bind(this));
};

OverlayUI.prototype.preloadImage = function(imgSrc) {
  var img = new Image();
  img.src = imgSrc;
};

OverlayUI.prototype.__setBackgroundImage = function(elem, imgSrc) {
  elem.css("background-image", "url(" + imgSrc + ")");
};

OverlayUI.prototype.__addItem = function(itemId, imgSrc, xPos, onClick) {
	var itemDiv = $('<div id="' + itemId + '"></div>');

	var item = {
		id: itemId,
		elem: itemDiv,
		imgSrc: imgSrc,
		xPos: xPos,
		onClick: onClick
	};
	this.__items.push(item);

	function cssPropertyValueSupported(prop, value) {
		var d = document.createElement('div');
		d.style[prop] = value;
		return d.style[prop] === value;
	}

	var parent = this;

	$("<img>", {src: imgSrc}).on("load", function() {
		item.elem.css("width", this.width);
		item.elem.css("height", this.height);

		if(DEBUG) {
			item.elem.css("box-sizing", "border-box");
			item.elem.css("border", "solid 1px red");
		} else {
			item.elem.css("pointer-events", "auto");
			// var maskSupported = false;
			// var maskSupported = createCssImage(item.elem, imgSrc, this.LOGO_ICON_WIDTH, this.HEIGHT);
			// if(maskSupported) itemDiv.css("background-color", "white");
			item.elem.css("background-image", "url(" + imgSrc + ")");
	    item.elem.css("background-color", "transparent");

			// if(maskSupported) {
			// 	itemDiv.mouseover(function() {
			// 		itemDiv.css("background-color", "#7b369e");//"rgb(255, 131, 247)");
			// 		itemDiv.css("background-color", "#7b369e");//"rgb(255, 131, 247)");
			// 	}.bind(this)).mouseout(function() {
			// 		itemDiv.css("background-color", "white");
			// 	}.bind(this));
			// }
		}

		parent.__positionItems(); // now that we have sizes
	});

	// itemDiv.css("transition", "top .2s, background-color .15s linear");
	itemDiv.css("transition", "top 0.3s cubic-bezier(.25,.8,.25,1), background-color cubic-bezier(.25,.8,.25,1)");
	itemDiv.css("cursor", "pointer");

	itemDiv.click(item.onClick.bind(this));
	this.container.append(itemDiv);
};

OverlayUI.prototype.__setupHandlers = function() {
	$('body').mouseout(this.__onMouseOut.bind(this)); // bc iframe
	$(document).mousemove(this.__onDocumentMouseMove.bind(this));
	$(this.interactiveImage).change(this.__onInteractiveImageChange.bind(this));
};

OverlayUI.prototype.__destroyHandlers = function() {
	$('body').unbind("mouseout");
	$(document).unbind("mousemove");
	$(this.interactiveImage).unbind("change");
};

OverlayUI.prototype.__onMouseOut = function(e) {
  if(e.toElement != null) return; // toElement will be null if out of iframe
	this.hide();
};

OverlayUI.prototype.__onInteractiveImageChange = function() {
	this.bounds = this.interactiveImage.getBounds();
	this.__positionAndSizeUIContainer();
	this.__positionItems();
  this.__positionAndSizeUIContainer();
};

OverlayUI.prototype.__onDocumentMouseMove = function(e) {
	if(!this.bounds) return;

	var mousePoint = new Vector(e.pageX, e.pageY);

  var imageMousePos = this.interactiveImage.pageToImageCoords(mousePoint.x, mousePoint.y);
  var posAbs = this.interactiveImage.imageToCanvasCoords(imageMousePos.x, imageMousePos.y);
  var bounds = this.interactiveImage.getBounds();

	var withinBounds = mousePoint.withinRect(this.bounds);

  var inBounds = true;
  if(!(posAbs.x <= bounds.width && posAbs.x >= 0)) {
    inBounds = false;
  }

  if(!(posAbs.y <= bounds.height && posAbs.y >= 0)) {
    inBounds = false;
  }

  //if(this.interactiveImage.toolbox) {
    //var tool = this.interactiveImage.toolbox.activeTool;

    //if(tool.name == 'VIEW') {
      //if(tool.hoverTooltip || tool.hoverCanvas) {
      //} else {
        //inBounds = false;
      //}
    //}
  //}

	if(inBounds) {
		this.show();
	} else {
		this.hide();
	}
}

OverlayUI.prototype.show = function() {
	if(!DEBUG) this.container.css("opacity", "1");
	var containerPos = $(this.interactiveImage.canvas).position();
	$.each(this.__items, function(i, item) {
		var yPos = (this.container.height() - item.elem.height()) / 2;
		item.elem.css("top", yPos + 5);
	}.bind(this));
}

OverlayUI.prototype.hide = function(immediate) {
  if(!DEBUG)	{
    this.container.css("opacity", "0");
  }

	$.each(this.__items, function(i, item) {
		var yPos = this.container.height();
		item.elem.css("top", yPos);
	}.bind(this));
};

OverlayUI.prototype.__logoClick = function() {
  window.open("https://pixorize.com", "_blank");
};

OverlayUI.prototype.__lightClick = function(e) {
	this.interactiveImage.annotator.show = !this.interactiveImage.annotator.show;

	if(this.interactiveImage.annotator.show) {
		this.__setBackgroundImage($(e.target), this.LIGHT_ON_SRC);
	} else {
		this.__setBackgroundImage($(e.target), this.LIGHT_OFF_SRC);
	}

	$(this.interactiveImage.annotator).triggerHandler("change");
};

OverlayUI.prototype.__fullscreenClick = function() {
  $('#interactive-image')[0].webkitRequestFullscreen();
};

OverlayUI.prototype.__zoomInClick = function() {
	if(this.isMobile) {
		this.interactiveImage.scroller.canScroll = true;
		this.interactiveImage.zoomer.setZoom(this.interactiveImage.zoomer.zoom*1.2);
		this.interactiveImage.scroller.canScroll = false;
	} else {
		this.interactiveImage.zoomer.setZoom(this.interactiveImage.zoomer.zoom*1.2);
	}
};

OverlayUI.prototype.__zoomOutClick = function() {
	if(this.isMobile) {
		this.interactiveImage.scroller.canScroll = true;
		this.interactiveImage.zoomer.setZoom(this.interactiveImage.zoomer.zoom*.8);
			this.interactiveImage.scroller.canScroll = false;
	} else {
		this.interactiveImage.zoomer.setZoom(this.interactiveImage.zoomer.zoom*.8);
	}
};

OverlayUI.prototype.__positionAndSizeUIContainer = function() {
  var canvas = $(this.interactiveImage.canvas);
  //var offset = canvas.offset();
  //var rect = {
    //left: offset.left,
    //top: offset.top,
    //right: offset.left + canvas.width(),
    //bottom: offset.top + canvas.height()
  //}

  this.container.css("position", "absolute");
	this.container.css("top", (canvas.height() - this.container.height()) + "px");
	this.container.css("left", 0);
	this.container.css("width", (canvas.width()) + "px");

	//var bottomOfImage = this.bounds.y + this.bounds.height;
	//var bottomEdgeOfScreen = $(window).scrollTop() + $(window).height();
	//var bottom = Math.min(bottomOfImage, bottomEdgeOfScreen);
  //bottom = bottomOfImage;

	//var widthOfImage = this.bounds.width;
	//var widthOfWindow = $(window).width();
	//var width = Math.min(widthOfImage, widthOfWindow);

  //this.container.css("position", "absolute");
	//this.container.css("top", bottom - this.container.height());
  //this.container.css("top", this.bounds.height + this.container.height()/2);// - this.container.height()/2);
	//this.container.css("left", this.bounds.x);
	//this.container.css("width", width + "px");
};

export default OverlayUI;
