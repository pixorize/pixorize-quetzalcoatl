import $ from 'jquery';
import _ from 'lodash';

var PLUS = 187;
var MINUS = 189;
var ZERO = 48;

var Zoomer = function(interactiveImage) {
	this.interactiveImage = interactiveImage;
	$(document).on("keydown", this.__onDocumentKeyDown.bind(this));
	$(document).on("keypress", this.__onDocumentKeyPress.bind(this));
};

Zoomer.prototype.__onDocumentKeyDown = function(e) {
	if(document.activeElement != document.body) return;

	if(e.keyCode == PLUS) this.setZoom(this.zoom * 2);
	if(e.keyCode == MINUS) this.setZoom(this.zoom * .5);
};

Zoomer.prototype.__onDocumentKeyPress = function(e) {
	if(e.keyCode == ZERO) this.setZoom(this.origZoom);
};

Zoomer.prototype.setOrigAnnotations = function(annotations) {
	this.origAnnotations = _.cloneDeep(annotations);
};

Zoomer.prototype.getMaxZoom = function() {
  var canvas = $(this.interactiveImage.canvas);
  var img = this.interactiveImage.img;
  var maxZoom = Math.max(canvas.width() / img.width, canvas.height() / img.height);
  return maxZoom;
};

Zoomer.prototype.checkMaxZoom = function() {
  var maxZoom = this.getMaxZoom();

  if(this.zoom < maxZoom) {
    this.setZoom(maxZoom);
  }
};

Zoomer.prototype.setZoom = function(zoom) {
	if(this.zoom == zoom) return;
	if(!this.zoom) this.origZoom = zoom;

  var canvas = $(this.interactiveImage.canvas);
  var img = this.interactiveImage.img;
  var maxWidth = Math.max(canvas.width() / img.width, canvas.height() / img.height);
  //console.log(canvas.width());
  //console.log(img.width);

	var oldZoom = this.zoom;
  zoom = Math.max(maxWidth, zoom);
	this.zoom = zoom;

	if(this.interactiveImage.scroller) {
		var bounds = this.interactiveImage.getBounds();
		var scrollX = this.interactiveImage.scroller.scrollX;
		var scrollY = this.interactiveImage.scroller.scrollY;

		var xCenterAsProportion = (scrollX + bounds.width/2)/oldZoom;
		var yCenterAsProportion = (scrollY + bounds.height/2)/oldZoom;
	}

	if(this.interactiveImage.annotator) this.updateAnnotator(zoom, oldZoom);
	this.__updateImage(true);

	if(this.interactiveImage.scroller) {
		var bounds = this.interactiveImage.getBounds();

		var newCenterX = xCenterAsProportion*zoom;
		var newCenterY = yCenterAsProportion*zoom;

		var distX = newCenterX - bounds.width/2;
		var distY = newCenterY - bounds.height/2;

		this.interactiveImage.scroller.setScroll(distX, distY);
	}

	this.interactiveImage.__isDirty = true;
	this.interactiveImage.draw();
};

Zoomer.prototype.updateAnnotator = function(zoom, oldZoom) {
	$.each(this.interactiveImage.annotator.annotations, function(i, annotation) {
		var annotation = this.interactiveImage.annotator.annotations[i];
		if(annotation.x) {
			annotation.x = this.origAnnotations[i].x*this.zoom;
			annotation.y = this.origAnnotations[i].y*this.zoom;
		} else {
			if(annotation.type == "DOT") {
				var newX = zoom/oldZoom*annotation.pos.x;
				var newY = zoom/oldZoom*annotation.pos.y;
				annotation.setPosition(newX, newY);
			} else if(annotation.type == "POLY" || annotation.type == "CIRCLE") {
				annotation.zoom(zoom/oldZoom);
			}
		}
	}.bind(this));
};

Zoomer.prototype.__updateImage = function(dontUpdate) {
	var width = this.interactiveImage.img.width*this.zoom;
	var height = this.interactiveImage.img.height*this.zoom;
	//this.interactiveImage.setSize(50, 50);

	if(this.interactiveImage.img && this.interactiveImage.scroller) {
		var zoom = 1;
		if(this.interactiveImage.zoomer) zoom = this.interactiveImage.zoomer.zoom;
		this.interactiveImage.scroller.setBounds(this.interactiveImage.img.width*zoom - this.interactiveImage.width, this.interactiveImage.img.height*zoom - this.interactiveImage.height);
	}
  //this.interactiveImage.setSize(width, height);

	if(this.interactiveImage.positioner) this.interactiveImage.positioner.position(dontUpdate);
};

export default Zoomer;
