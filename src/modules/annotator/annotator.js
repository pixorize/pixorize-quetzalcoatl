import $ from 'jquery';

import PointAnnotation from './annotation/point-annotation'; 
import CircleAnnotation from './annotation/circle-annotation'; 
import PolyAnnotation from './annotation/poly-annotation'; 

import LayerManager from '../../manager/layer-manager';

var Annotator = function(annotations, interactiveImage) {
	this.interactiveImage = interactiveImage;
  this.__loadAnnotations(annotations);

	$(document).on("mousemove", this.__onDocumentMouseMove.bind(this));
	$(this.interactiveImage.canvas).on("mousedown", this.__onCanvasMouseDown.bind(this));
	$(document).on("mouseup", this.__onDocumentMouseUp.bind(this));
	$(this.interactiveImage.canvas).on("dblclick", this.__onCanvasDblClick.bind(this));
	$(this.interactiveImage.scroller).on("change", this.__onScrollerChange.bind(this));

	this.show = true;
	this.layerManager = new LayerManager();
};

Annotator.prototype.destroy = function(e) {
  $.each(this.annotations, function(i, annotation) {
    $(annotation).off("change");
    annotation.delete();
  });

	$(document).off("mousemove");
	$(this.interactiveImage.canvas).off("mousedown");
	$(document).off("mouseup");
	$(this.interactiveImage.canvas).off("dblclick");
	$(this.interactiveImage.scroller).off("change");
};

/*
* Annotations
*/

Annotator.prototype.serialize = function(zoom) {
	var annotations = [];

	$.each(this.annotations, function(i, annotation) {
		annotations.push(annotation.serialize(zoom));
	});

	return annotations;
};

Annotator.prototype.__loadAnnotations = function(annotationObjs) {
  this.annotations = [];

  $.each(annotationObjs, function(i, annotationObj) {
		var annotationType = annotationObj.type ? annotationObj.type : "DOT";

		var annotation;
		if(annotationType == "DOT") {
			annotation = new PointAnnotation(annotationObj.x, annotationObj.y, annotationObj.text);
		} else if(annotationType == "CIRCLE") {
			annotation = new CircleAnnotation(annotationObj.x, annotationObj.y, annotationObj.radius, annotationObj.text);
		} else if(annotationType == "POLY") {
			annotation = new PolyAnnotation(annotationObj.points, annotationObj.text);
		}

		this.addAnnotation(annotation);
	}.bind(this));
};

Annotator.prototype.addAnnotation = function(annotation, atFront) {
	if(atFront) {
		this.annotations.unshift(annotation);
	} else {
		this.annotations.push(annotation);
	}

	$(annotation).on("change", this.__onAnnotationChange.bind(this));
	$(this).triggerHandler("change");
};

Annotator.prototype.deleteAnnotation = function(annotation) {
	var index = this.annotations.indexOf(annotation);
	if(index == -1) return;

	this.annotations.splice(index, 1);
	annotation.delete();
	$(this).triggerHandler("change");
};

/*
* Drawing
*/

Annotator.prototype.draw = function() {
	if(!this.show) return;

	var reversed = _.clone(this.annotations);
	reversed.reverse();
	this.layerManager.sort(reversed);
	$.each(reversed, function(i, annotation) {
		var canvasPos = this.interactiveImage.imageToCanvasCoords(annotation.pos.x, annotation.pos.y);
    annotation.draw(this.interactiveImage.context, canvasPos, this.interactiveImage);
  }.bind(this));
};

/*
* Events
*/

Annotator.prototype.__onScrollerChange = function(e) {
	$(this).triggerHandler("change");
};

Annotator.prototype.__onAnnotationChange = function(e) {
	$(this).triggerHandler("change"); // bubble up event
};

/*
* I/O
*/

Annotator.prototype.__onDocumentMouseMove = function(e) {
};

Annotator.prototype.__onCanvasMouseDown = function(e) {
};

Annotator.prototype.__onDocumentMouseUp = function(e) {
};

Annotator.prototype.__onCanvasDblClick = function(e) {
};

export default Annotator;
