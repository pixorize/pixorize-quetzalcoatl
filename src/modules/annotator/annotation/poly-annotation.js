import $ from 'jquery';
import pointInPolygon from 'point-in-polygon';

import Annotation from './annotation';
import PolyDrawer from '../drawer/poly-drawer';

import Vector from '../../../lib/math/vector';
import Polygon from '../../../lib/math/polygon';

var pd;

var PolyAnnotation = function(points, text) {
	Annotation.call(this, 0, 0, text); // super();

  this.__polyDrawer = new PolyDrawer();
	pd = this.__polyDrawer;

	var newPoints = [];
	$.each(points, function(i, point) {
		newPoints.push(new Vector(point.x, point.y));
	});
	this.polygon = new Polygon(newPoints);

  this.type = "POLY";
	this.hoveredPoint = null;
	this.draggedPoint = null;
};

PolyAnnotation.prototype = Object.create(Annotation.prototype);
PolyAnnotation.prototype.constructor = Annotation;

PolyAnnotation.prototype.zoom = function(scale) {
	$.each(this.polygon.points, function(i, point) {
		point.x *= scale;
		point.y *= scale;
	}.bind(this));
}

PolyAnnotation.prototype.addPosition = function(dx, dy) {
	if(dx == 0 && dy == 0) return;

	$.each(this.polygon.points, function(i, point) {
		point.x += dx;
		point.y += dy;
	}.bind(this));

	$(this).triggerHandler("change");
}

PolyAnnotation.prototype.setPosition = function(x, y) {
	var dx = x - this.pos.x;
	var dy = y - this.pos.y;

	$.each(this.polygon.points, function(i, point) {
		point.x += dx;
		point.y += dy;
	}.bind(this));

	$(this).triggerHandler("change");
}

PolyAnnotation.prototype.onDrag = function(lastPos) {
	if(this.isDragging) {
		var dx = this.pos.x - lastPos.x;
		var dy = this.pos.y - lastPos.y;
	}

	$(this).triggerHandler("change");
};

PolyAnnotation.prototype.getBB = function() {
	var minX = 99999999;
	var minY = minX;
	var maxX = -minX;
	var maxY = -minX;

	$.each(this.polygon.points, function(i, point) {
		minX = Math.min(minX, point.x);
		maxX = Math.max(maxX, point.x);
		minY = Math.min(minY, point.y);
		maxY = Math.max(maxY, point.y);
	});

	var width = maxX-minX;
	var height = maxY-minY;

	var rect = {
		x: minX+width/2,
		y: minY+height/2,
		width: width,
		height: height
	};

	var vertexRadius = this.__polyDrawer.VERTEX_OUTER_RADIUS;

	rect.width += 2*vertexRadius;
	rect.height += 2*vertexRadius;

	return rect;
};

PolyAnnotation.prototype.setHoveredPoint = function(point) {
	if(this.hoveredPoint == point) return;
	// console.log("setHoveredPoint");
	this.hoveredPointIndex = this.polygon.points.indexOf(point);
	this.hoveredPoint = point;
	$(this).triggerHandler("change");
};

PolyAnnotation.prototype.setDraggedPoint = function(point) {
	if(this.draggedPoint == point) return;
	// console.log("setDraggedPoint");
	this.draggedPointIndex = this.polygon.points.indexOf(point);
	this.draggedPoint = point;
	$(this).triggerHandler("change");
};

PolyAnnotation.prototype.draw = function(context, canvasPos, interactiveImage) {
  Annotation.prototype.draw.call(this, context, canvasPos);  // super.draw();

	var color = this.__polyDrawer.LINE_COLOR;
	if(this.isHovered) color = this.__polyDrawer.HOVERED_LINE_COLOR;
	if(this.isSelected) color = this.__polyDrawer.SELECTED_LINE_COLOR;

	var newPoints = [];
	$.each(this.polygon.points, function(i, point) {
		var canvasPos = interactiveImage.imageToCanvasCoords(point.x, point.y);
		newPoints.push(new Vector(canvasPos.x, canvasPos.y));
	}.bind(this));

  // this.__polyDrawer.drawPoly(context, newPoints, color, this.__polyDrawer.POINT_FILL_COLOR, this.__polyDrawer.POINT_STROKE_COLOR);
  this.__polyDrawer.drawPoly(context, newPoints, this.isHovered, this.isSelected, this.hoveredPointIndex, this.draggedPointIndex);// color, this.__polyDrawer.POINT_FILL_COLOR, this.__polyDrawer.POINT_STROKE_COLOR);
};

PolyAnnotation.prototype.pointWithinObj = function(point) {
	var points = [];
	var withinVertices = false;

	$.each(this.polygon.points, function(i, vertex) {
		points.push([vertex.x, vertex.y]);

		if(vertex.vectorIsWithinDist(point, this.__polyDrawer.VERTEX_OUTER_RADIUS)) {
			withinVertices = true;
			return false;
		}
	}.bind(this));

	return withinVertices || pointInPolygon([point.x, point.y], points);
	// return withinVertices || this.polygon.doesContainsPoint(point);
};

PolyAnnotation.prototype.serialize = function(zoom) {
	var pointsScaled = _.clone(this.polygon.points);
	$.each(pointsScaled, function(i, point) {
		point.x /= zoom;
		point.y /= zoom;
	});

	var obj = {x: this.pos.x, y: this.pos.y, points: pointsScaled, text: this.text, type: this.type};
	return obj;
};

export default PolyAnnotation;
