import Annotation from './annotation';
import Drawer from '../drawer/drawer';

var CircleAnnotation = function(x, y, radius, text) {
	Annotation.call(this, x, y, text); // super();

  this.radius = radius;
	this.type = "CIRCLE";

  this.__drawer = new Drawer();
};

CircleAnnotation.prototype = Object.create(Annotation.prototype);
CircleAnnotation.prototype.constructor = Annotation;

CircleAnnotation.prototype.draw = function(context, canvasPos, interactiveImage) {
  Annotation.prototype.draw.call(this, context, canvasPos);  // super.draw();

	var outerStrokeStyle
	var innerStrokeStyle;

	if(this.isSelected) {
		outerStrokeStyle = "rgba(20, 119, 65, 1)";
		innerStrokeStyle = "rgba(178, 241, 130, 1)";
	} else if(this.isHovered) {
		outerStrokeStyle = "rgba(133, 52, 122, 1)";
		innerStrokeStyle = "rgba(255, 131, 247, 1)";
	} else {
		outerStrokeStyle = "rgba(0, 124, 189, 1)";
		innerStrokeStyle = "rgba(131, 211, 255, 1)";
	}

	this.__drawer.context = context;
	this.__drawer.drawDoubleCircle(
		canvasPos.x, canvasPos.y,
		this.radius,
		{strokeStyle: outerStrokeStyle, lineWidth: 3},
		{strokeStyle: innerStrokeStyle, lineWidth: 1}
	);
	// this.__dotDrawer.drawDot(context, canvasPos.x, canvasPos.y, this.isHovered, this.isSelected);
};

CircleAnnotation.prototype.zoom = function(scale) {
	this.setPosition(this.pos.x*scale, this.pos.y*scale);
	this.radius *= scale;
}

CircleAnnotation.prototype.pointWithinObj = function(point) {
	return this.pos.vectorWithinDist(point, this.radius);
}

CircleAnnotation.prototype.getBB = function() {
	return {
		x: this.pos.x,
		y: this.pos.y,
		width: this.radius*2,
		height: this.radius*2
	};
};

CircleAnnotation.prototype.serialize = function(zoom) {
	var obj = {x: this.pos.x/zoom, y: this.pos.y/zoom, radius: this.radius/zoom, text: this.text, type: this.type};
	return obj;
};

export default CircleAnnotation;
