import $ from 'jquery';
import Vector from '../../../lib/math/vector';

var Annotation = function(x, y, text) {
  this.pos = new Vector(x, y);
  this.text = text;

  this.isHovered = false;
  this.isSelected = false;
  this.isDragging = false;

  this.type = "DEFAULT";
};

Annotation.prototype.setPosition = function(x, y) {
  this.pos.x = x;
  this.pos.y = y;
};

Annotation.prototype.addPosition = function(dx, dy) {
  this.pos.x += dx;
  this.pos.y += dy;
}

Annotation.prototype.setText = function(text) {
  this.text = text;
  $(this).triggerHandler("change");
};

/*
* save/load
*/

Annotation.prototype.serialize = function() {
};

/*
* draw
*/

Annotation.prototype.draw = function(context, canvasPos, interactiveImage) {
};

/*
* I/O
*/

Annotation.prototype.onCanvasMouseDown = function(imageMousePos) {
}

Annotation.prototype.onDocumentMouseUp = function(imageMousePos) {
}

Annotation.prototype.onImageMouseMove = function(imageMousePos, imageBounds) {
};

/*
* drag/drop
*/

Annotation.prototype.startDrag = function() {
  if(this.isDragging) return;

  this.isDragging = true;
  $(this).triggerHandler("change");
}

Annotation.prototype.stopDrag = function() {
  if(!this.isDragging) return;

  this.isDragging = false;
  $(this).triggerHandler("change");
}

Annotation.prototype.onDrag = function(lastPos) {
};

Annotation.prototype.getBB = function() {
};

/*
* select/deselect
*/

Annotation.prototype.select = function() {
  if(this.isSelected) return; // do nothing if already selected

  this.isSelected = true;
  $(this).triggerHandler("change");
};

Annotation.prototype.deselect = function() {
  if(!this.isSelected) return; // do nothing if already deselected

  this.isSelected = false;
  $(this).triggerHandler("change");
};

Annotation.prototype.pointWithinObj = function(point) {
}

/*
* hover/dehover
*/

Annotation.prototype.hover = function() {
  if(this.isHovered) return; // do nothing if already hovered

  this.isHovered = true;
  $(this).triggerHandler("change");
};

Annotation.prototype.dehover = function() {
  if(!this.isHovered) return; // do nothing if already dehovered

  this.isHovered = false;
  $(this).triggerHandler("change");
};

/*
* delete
*/

Annotation.prototype.delete = function() {
  $(this).triggerHandler("delete");
};

export default Annotation;
