import Annotation from './annotation';
import PointDrawer from '../drawer/point-drawer';

var PointAnnotation = function(x, y, text) {
	Annotation.call(this, x, y, text); // super();

  this.__pointDrawer = new PointDrawer();
  this.radius = this.__pointDrawer.OUTER_DOT_RADIUS;

	this.type = "DOT";
};

PointAnnotation.prototype = Object.create(Annotation.prototype);
PointAnnotation.prototype.constructor = Annotation;

PointAnnotation.prototype.draw = function(context, canvasPos, interactiveImage) {
  Annotation.prototype.draw.call(this, context, canvasPos);  // super.draw();

	this.__pointDrawer.drawPoint(context, canvasPos.x, canvasPos.y, this.isHovered, this.isSelected);
};

PointAnnotation.prototype.pointWithinObj = function(point) {
	var radius = this.radius;
	var isMobile = /Mobi|Tablet|iPad|iPhone/.test(navigator.userAgent);
	if(isMobile) radius *= 2; // Make it twice as easy to click

	return this.pos.vectorWithinDist(point, radius);
}

PointAnnotation.prototype.getBB = function() {
	return {
		x: this.pos.x,
		y: this.pos.y,
		width: this.radius*2,
		height: this.radius*2
	};
};

PointAnnotation.prototype.serialize = function(zoom) {
	var obj = {x: this.pos.x/zoom, y: this.pos.y/zoom, text: this.text, type: this.type};
	return obj;
};

export default PointAnnotation;
