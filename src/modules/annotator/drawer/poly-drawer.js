import $ from 'jquery';
import Drawer from './drawer';

var PolyDrawer = function() {
  // The lines of the poly
  this.IDLED_OUTER_STROKE_STYLE = "rgba(0, 124, 189, 1)";
  this.IDLED_INNER_STROKE_STYLE = "rgba(131, 211, 255, 1)";

  this.HOVERED_OUTER_STROKE_STYLE = "rgba(133, 52, 122, 1)";
  this.HOVERED_INNER_STROKE_STYLE = "rgba(255, 131, 247, 1)";

  this.SELECTED_OUTER_STROKE_STYLE = "rgba(20, 119, 65, 1)";
  this.SELECTED_INNER_STROKE_STYLE = "rgba(178, 241, 130, 1)";

  this.OUTER_LINE_WIDTH = 3;
  this.INNER_LINE_WIDTH = 1;

  this.IDLED_OUTER_LINE_STYLE = {
    strokeStyle: this.IDLED_OUTER_STROKE_STYLE,
    lineWidth: this.OUTER_LINE_WIDTH,
    lineCap: "square"
  };

  this.IDLED_INNER_LINE_STYLE = {
    strokeStyle: this.IDLED_INNER_STROKE_STYLE,
    lineWidth: this.INNER_LINE_WIDTH,
    lineCap: "square"
  };

  this.HOVERED_OUTER_LINE_STYLE = {
    strokeStyle: this.HOVERED_OUTER_STROKE_STYLE,
    lineWidth: this.OUTER_LINE_WIDTH,
    lineCap: "square"
  };

  this.HOVERED_INNER_LINE_STYLE = {
    strokeStyle: this.HOVERED_INNER_STROKE_STYLE,
    lineWidth: this.INNER_LINE_WIDTH,
    lineCap: "square"
  };

  this.SELECTED_OUTER_LINE_STYLE = {
    strokeStyle: this.SELECTED_OUTER_STROKE_STYLE,
    lineWidth: this.OUTER_LINE_WIDTH,
    lineCap: "square"
  };

  this.SELECTED_INNER_LINE_STYLE = {
    strokeStyle: this.SELECTED_INNER_STROKE_STYLE,
    lineWidth: this.INNER_LINE_WIDTH,
    lineCap: "square"
  };

  // ---------------------------------------------

  this.IDLED_LINE_COLOR = "black";
  this.HOVERED_LINE_COLOR = "rgb(133, 52, 122)";
  this.SELECTED_LINE_COLOR = "rgb(178, 241, 130)";

  // The vertices of the poly (i.e. the points)
  this.VERTEX_OUTER_RADIUS = 5;
  this.VERTEX_INNER_RADIUS = .6 * this.VERTEX_OUTER_RADIUS;

	this.IDLED_VERTEX_OUTER_FILL_STYLE = "rgba(0, 124, 189, .75)";
	this.IDLED_VERTEX_INNER_FILL_STYLE = "rgba(131, 211, 255, .9)";

	this.HOVERED_VERTEX_OUTER_FILL_STYLE = "rgba(133, 52, 122, .75)";
	this.HOVERED_VERTEX_INNER_FILL_STYLE = "rgba(255, 131, 247, .9)";

	this.SELECTED_VERTEX_OUTER_FILL_STYLE = "rgba(20, 119, 65, .9)";
	this.SELECTED_VERTEX_INNER_FILL_STYLE = "rgba(178, 241, 130, .75)";

  this.FIRST_VERTEX_SCALE = 2; // the first vertex is "scale" times bigger

  // Intersection "X" (when you're drawing a poly line that isn't allowed)
  this.X_WIDTH = this.X_HEIGHT = 10;
  this.X_STROKE_STYLE = "red";
  this.X_LINE_WIDTH = 2;

  // private vars
  this.__drawer = new Drawer();
};

PolyDrawer.prototype.drawVertex = function(context, x, y, isHovered, isSelected, vertexIsHovered, vertexIsDragged) {
  var outerColor = this.IDLED_VERTEX_OUTER_FILL_STYLE;
	var innerColor = this.IDLED_VERTEX_INNER_FILL_STYLE;

	if(isHovered || vertexIsHovered) {
		outerColor = this.HOVERED_VERTEX_OUTER_FILL_STYLE;
		innerColor = this.HOVERED_VERTEX_INNER_FILL_STYLE;
	}

	if(isSelected || vertexIsDragged) {
		outerColor = this.SELECTED_VERTEX_OUTER_FILL_STYLE;
		innerColor = this.SELECTED_VERTEX_INNER_FILL_STYLE;
	}

	this.__drawer.context = context;
  this.__drawer.drawConcentricCircles(
		x, y,
		this.VERTEX_OUTER_RADIUS, this.VERTEX_INNER_RADIUS,
		{fillStyle: outerColor}, {fillStyle: innerColor}
	);
};

// The first vertex (i.e. point) created when drawing a poly
PolyDrawer.prototype.drawFirstVertex = function(context, x, y) {
  this.drawVertex(context, x, y, false, true, false, false);
  return;
  this.__drawer.context = context;
  this.__drawer.drawConcentricCircles(
    x, y,
    this.VERTEX_OUTER_RADIUS*this.FIRST_VERTEX_SCALE, this.VERTEX_INNER_RADIUS*this.FIRST_VERTEX_SCALE,
    {fillStyle: this.SELECTED_VERTEX_OUTER_FILL_STYLE}, {fillStyle: this.SELECTED_VERTEX_INNER_FILL_STYLE}
  );
}

PolyDrawer.prototype.drawLineSegment = function(context, startPoint, endPoint, isHovered, isSelected) {
  var lineColor = this.IDLED_LINE_COLOR;

  var outerStrokeStyle = this.IDLED_OUTER_STROKE_STYLE;
  var innerStrokeStyle = this.IDLED_INNER_STROKE_STYLE;

  if(isHovered) {
		lineColor = this.HOVERED_LINE_COLOR;
	}

	if(isSelected) {
		lineColor = this.SELECTED_LINE_COLOR;
	}

  this.__drawer.context = context;
  this.__drawer.drawLineSegment(startPoint, endPoint, {
    strokeStyle: lineColor,
    lineWidth: 1
  });
};

// This is the "X" that pops up when moving a poly's point that causes lines to cross
PolyDrawer.prototype.drawIntersection = function(context, point) {
  this.__drawer.context = context;
  this.__drawer.drawX(
    point.x, point.y,
    this.X_WIDTH, this.X_HEIGHT,
    {
      lineWidth: this.X_LINE_WIDTH,
      strokeStyle: this.X_STROKE_STYLE
    }
  );
};

// Draws the whole thing
PolyDrawer.prototype.drawPoly = function(context, poly, isHovered, isSelected, hoverPointIndex, draggedPointIndex) {
  this.__drawer.context = context;

  var outerLineStyle = this.IDLED_OUTER_LINE_STYLE;
  var innerLineStyle = this.IDLED_INNER_LINE_STYLE;

  if(isHovered) {
    outerLineStyle = this.HOVERED_OUTER_LINE_STYLE;
    innerLineStyle = this.HOVERED_INNER_LINE_STYLE;
  }

  if(isSelected) {
    outerLineStyle = this.SELECTED_OUTER_LINE_STYLE;
    innerLineStyle = this.SELECTED_INNER_LINE_STYLE;
  }

  this.__drawer.drawDoublePoly(poly, outerLineStyle, innerLineStyle);
  this.drawVertices(context, poly, isHovered, isSelected, hoverPointIndex, draggedPointIndex);
};

PolyDrawer.prototype.__drawLines = function(context, poly, isHovered, isSelected) {
  // Draw lines b/w points i and i+1
  $.each(poly, function(i, thisPoint) {
    if(poly.length > 1 && i != poly.length-1) {
      var nextPoint = poly[i+1];
      this.drawLineSegment(context, thisPoint, nextPoint, isHovered, isSelected)
    }
  }.bind(this));

  // Connect the last and the first points
  this.drawLineSegment(context, poly[poly.length-1], poly[0], isHovered, isSelected);
};

PolyDrawer.prototype.drawVertices = function(context, poly, isHovered, isSelected, hoverPointIndex, draggedPointIndex) {
  $.each(poly, function(i, point) {
    this.drawVertex(context, point.x, point.y, isHovered, isSelected, i==hoverPointIndex, i==draggedPointIndex);
  }.bind(this));
};

// debug

PolyDrawer.prototype.drawSlopeIntercept = function(context, line, lol) {
  var startPoint = {x: 0, y: line.b};
  var endPoint = {x: 500, y: line.m*500 + line.b};

  var sP = ii.imageToCanvasCoords(startPoint.x, startPoint.y);
  var eP = ii.imageToCanvasCoords(endPoint.x, endPoint.y);

  this.drawLineSegment(context, sP, eP, false, false);
};

export default PolyDrawer;
