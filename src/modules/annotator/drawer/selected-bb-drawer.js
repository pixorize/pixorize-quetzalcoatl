import Drawer from './drawer';

var SelectedBBDrawer = function() {
  this.BB_STROKE_STYLE = "rgb(65, 244, 226)";
  this.BB_LINE_WIDTH = 1;
  this.BB_LINE_DASH = [5];
  this.BB_PADDING = 10;

  // --------------------------------------------

  this.BB_OUTER_LINE_STROKE_STYLE = "#0e6b9c";
  this.BB_INNER_LINE_STROKE_STYLE = "#7bc9f5";

  this.BB_OUTER_LINE_WIDTH = 3;
  this.BB_INNER_LINE_WIDTH = 1;

  // --------------------------------------------

  this.DELETE_RADIUS = 8;
  this.IDLED_DELETE_FILL_STYLE = "rgba(255, 0, 0, .8)";
  this.HOVERED_DELETE_FILL_STYLE = "rgba(120, 0, 0, .8)";

  this.outerLineStyle = {
    strokeStyle: this.BB_OUTER_LINE_STROKE_STYLE,
    lineWidth: this.BB_OUTER_LINE_WIDTH,
    lineCap: "square"
  };

  this.innerLineStyle = {
    strokeStyle: this.BB_INNER_LINE_STROKE_STYLE,
    lineWidth: this.BB_INNER_LINE_WIDTH,
    lineCap: "square"
  };

  this.__drawer = new Drawer();
};

SelectedBBDrawer.prototype.drawBB = function(context, rect, deleteIsHovered) {
  this.drawRect(context, rect);
  this.drawDeleteCircle(context, rect, deleteIsHovered);
  this.drawDeleteCross(context, rect);
};

SelectedBBDrawer.prototype.drawRect = function(context, rect) {
  var topRight1 = {x: rect.x + rect.width/2 + this.BB_PADDING - this.DELETE_RADIUS, y: rect.y - rect.height/2 - this.BB_PADDING};
  var topLeft = {x: rect.x - rect.width/2 - this.BB_PADDING, y: rect.y - rect.height/2 - this.BB_PADDING};
  var bottomLeft = {x: rect.x - rect.width/2 - this.BB_PADDING, y: rect.y + rect.height/2 + this.BB_PADDING};
  var bottomRight = {x: rect.x + rect.width/2 + this.BB_PADDING, y: rect.y + rect.height/2 + this.BB_PADDING};
  var topRight2 = {x: rect.x + rect.width/2 + this.BB_PADDING, y: rect.y - rect.height/2 - this.BB_PADDING + this.DELETE_RADIUS};
  var points = [topRight1, topLeft, bottomLeft, bottomRight, topRight2];

  this.__drawer.context = context;

  context.setLineDash(this.BB_LINE_DASH);
  this.__drawer.drawDoubleConnectedPoints(points, this.outerLineStyle, this.innerLineStyle);
  context.setLineDash([]);
}

SelectedBBDrawer.prototype.getDeleteButtonPos = function(rect) {
  /*
  * Returns the position of the "delete button"
  */

  var deleteX = rect.x + rect.width/2 + this.BB_PADDING;
  var deleteY = rect.y + -rect.height/2 - this.BB_PADDING;
  return new Vector(deleteX, deleteY);
}

SelectedBBDrawer.prototype.drawDeleteCircle = function(context, rect, deleteIsHovered) {
  var x = rect.x;
  var y = rect.y;

  var fillStyle = this.IDLED_DELETE_FILL_STYLE;
  if(deleteIsHovered) fillStyle = this.HOVERED_DELETE_FILL_STYLE;

  context.beginPath();
  context.arc(x+rect.width/2+this.BB_PADDING, y-rect.height/2-this.BB_PADDING, this.DELETE_RADIUS, 0, 2*Math.PI);
  context.fillStyle = fillStyle;
  context.fill();
  context.closePath();
}

SelectedBBDrawer.prototype.drawDeleteCross = function(context, rect) {
  var x = rect.x;
  var y = rect.y;

  context.strokeStyle = "white";
  context.lineWidth = 2;

  var percentToEdge = .7;

  context.beginPath();
  context.moveTo(x + rect.width/2 + this.BB_PADDING - this.DELETE_RADIUS/Math.sqrt(2)*percentToEdge, y - rect.height/2 - this.BB_PADDING - this.DELETE_RADIUS/Math.sqrt(2)*percentToEdge);
  context.lineTo(x + rect.width/2 + this.BB_PADDING + this.DELETE_RADIUS/Math.sqrt(2)*percentToEdge, y - rect.height/2 - this.BB_PADDING + this.DELETE_RADIUS/Math.sqrt(2)*percentToEdge);
  context.moveTo(x + rect.width/2 + this.BB_PADDING - this.DELETE_RADIUS/Math.sqrt(2)*percentToEdge, y - rect.height/2 - this.BB_PADDING + this.DELETE_RADIUS/Math.sqrt(2)*percentToEdge);
  context.lineTo(x + rect.width/2 + this.BB_PADDING + this.DELETE_RADIUS/Math.sqrt(2)*percentToEdge, y - rect.height/2 - this.BB_PADDING - this.DELETE_RADIUS/Math.sqrt(2)*percentToEdge);
  context.stroke();
  context.closePath();
}

export default SelectedBBDrawer;
