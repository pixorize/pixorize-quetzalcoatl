import Drawer from './drawer';

var PointDrawer = function() {
	this.OUTER_DOT_RADIUS = 10;
	this.INNER_DOT_RADIUS = .6 * this.OUTER_DOT_RADIUS;

	this.IDLED_DOT_OUTER_FILL_STYLE = "rgba(0, 124, 189, .75)";
	this.IDLED_DOT_INNER_FILL_STYLE = "rgba(131, 211, 255, .9)";

	this.HOVERED_DOT_OUTER_FILL_STYLE = "rgba(133, 52, 122, .75)";
	this.HOVERED_DOT_INNER_FILL_STYLE = "rgba(255, 131, 247, .9)";

	this.SELECTED_DOT_OUTER_FILL_STYLE = "rgba(20, 119, 65, .9)";
	this.SELECTED_DOT_INNER_FILL_STYLE = "rgba(178, 241, 130, .75)";

	this.__drawer = new Drawer();
};

PointDrawer.prototype.drawPointScaled = function(context, x, y, isHover, isActive, scale) {
	var outerColor = this.IDLED_DOT_OUTER_FILL_STYLE;
	var innerColor = this.IDLED_DOT_INNER_FILL_STYLE;

	if(isHover) {
		outerColor = this.HOVERED_DOT_OUTER_FILL_STYLE;
		innerColor = this.HOVERED_DOT_INNER_FILL_STYLE;
	}

	if(isActive) {
		outerColor = this.SELECTED_DOT_OUTER_FILL_STYLE;
		innerColor = this.SELECTED_DOT_INNER_FILL_STYLE;
	}

	this.__drawer.context = context;
  this.__drawer.drawConcentricCircles(
		x, y,
		this.OUTER_DOT_RADIUS*scale, this.INNER_DOT_RADIUS*scale,
		{fillStyle: outerColor}, {fillStyle: innerColor}
	);
};

PointDrawer.prototype.drawPoint = function(context, x, y, isHover, isActive) {
	var outerColor = this.IDLED_DOT_OUTER_FILL_STYLE;
	var innerColor = this.IDLED_DOT_INNER_FILL_STYLE;

	if(isHover) {
		outerColor = this.HOVERED_DOT_OUTER_FILL_STYLE;
		innerColor = this.HOVERED_DOT_INNER_FILL_STYLE;
	}

	if(isActive) {
		outerColor = this.SELECTED_DOT_OUTER_FILL_STYLE;
		innerColor = this.SELECTED_DOT_INNER_FILL_STYLE;
	}

	this.__drawer.context = context;
  this.__drawer.drawConcentricCircles(
		x, y,
		this.OUTER_DOT_RADIUS, this.INNER_DOT_RADIUS,
		{fillStyle: outerColor}, {fillStyle: innerColor}
	);
};

export default PointDrawer;
