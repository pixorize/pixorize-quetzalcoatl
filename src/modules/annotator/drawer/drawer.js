import $ from 'jquery';

/*
* Class used to draw common shapes on an html5 canvas element
*/

var Drawer = function(context) {
  if(context) this.context = context;

  this.__noContextError = "You must define a context before drawing:\nvar myDrawer = new Drawer(context);\nor\nmyDrawer.context = canvas.getContext('2d');";
}

/*
* Lines
*/

Drawer.prototype.drawLineSegment = function(startPoint, endPoint, style) {
  this.__startDraw(style);

  this.context.beginPath();
  this.context.moveTo(startPoint.x, startPoint.y);
  this.context.lineTo(endPoint.x, endPoint.y);
  this.__stroke(style);
  this.context.closePath();
}

Drawer.prototype.drawDoubleLineSegment = function(startPoint, endPoint, outerStyle, innerStyle) {
  this.drawLineSegment(startPoint, endPoint, outerStyle);
  this.drawLineSegment(startPoint, endPoint, innerStyle);
}

Drawer.prototype.drawDoublePoly = function(points, outerStyle, innerStyle) {
  this.drawPoly(points, outerStyle);
  this.drawPoly(points, innerStyle);
};

Drawer.prototype.drawPoly = function(points, style) {
  $.each(points, function(i, startPoint) {
    var endPoint = (i == points.length-1) ? points[0] : points[i+1];
    this.drawLineSegment(startPoint, endPoint, style);
  }.bind(this));
};

Drawer.prototype.drawConnectedPoints = function(points, style) {
  $.each(points, function(i, startPoint) {
    if(i == points.length -1) return false;
    var endPoint = points[i+1];

    this.drawLineSegment(startPoint, endPoint, style);
  }.bind(this));
};

Drawer.prototype.drawDoubleConnectedPoints = function(points, outerStyle, innerStyle) {
  this.drawConnectedPoints(points, outerStyle);
  this.drawConnectedPoints(points, innerStyle);
};

// This is coupled to the Pixorize "Line" class
Drawer.prototype.drawLine = function(line, style) {
  line = line.extendPoints(999999);
  this.drawLineSegment(line.startPoint, line.endPoint, style);
}

/*
* Circles
*/

Drawer.prototype.drawCircle = function(x, y, radius, style) {
  this.__startDraw(style);

  this.context.beginPath();
	this.context.arc(x, y, radius, 0, 2*Math.PI);
  this.__stroke(style);
  this.__fill(style);
	this.context.closePath();
}

Drawer.prototype.drawDoubleCircle = function(x, y, radius, outerStyle, innerStyle) {
  this.drawCircle(x, y, radius, outerStyle);
  this.drawCircle(x, y, radius, innerStyle);
}

Drawer.prototype.drawConcentricCircles = function(x, y, outerRadius, innerRadius, outerStyle, innerStyle) {
  this.drawCircle(x, y, outerRadius, outerStyle);
  this.drawCircle(x, y, innerRadius, innerStyle);
}

/*
* Misc.
*/

Drawer.prototype.drawX = function(x, y, width, height, style) {
  this.__startDraw(style);

  var hw = width/2; // half width
  var hh = height/2; // half height

  this.context.beginPath();
  this.context.moveTo(x - hw, y - hw);
  this.context.lineTo(x + hw, y + hw);
  this.context.moveTo(x + hw, y - hw);
  this.context.lineTo(x - hw, y + hw);
  this.context.stroke();
  this.context.closePath();
};

/*
* Private functions
*/

Drawer.prototype.__startDraw = function(style) {
  if(!this.context) throw this.__noContextError;

  this.__resetStyle();
  this.__applyStyle(style);
}

Drawer.prototype.__applyStyle = function(style) {
  if(!style) return;
  if(style.fillStyle) this.context.fillStyle = style.fillStyle;
  if(style.strokeStyle) this.context.strokeStyle = style.strokeStyle;
  if(style.lineWidth) this.context.lineWidth = style.lineWidth;
  if(style.lineCap) this.context.lineCap = style.lineCap;

}

Drawer.prototype.__resetStyle = function(style) {
  this.context.fillStyle = null;
  this.context.strokeStyle = null;
  this.context.lineWidth = 0;
}

Drawer.prototype.__fill = function(style) {
  if(style && (style.fillStyle)) this.context.fill();
}

Drawer.prototype.__stroke = function(style) {
  if(style && (style.strokeStyle || style.lineWidth)) this.context.stroke();
}

export default Drawer;
