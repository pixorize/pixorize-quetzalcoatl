import $ from 'jquery';

var Scroller = function() {
	this.init();
};

Scroller.prototype.init = function() {
	this.scrollX = 0;
	this.scrollY = 0;
	this.behaviors = [];
	this.canScroll = true;
}

Scroller.prototype.setBounds = function(width, height) {
	this.boundX = width;
	this.boundY = height;
};

Scroller.prototype.setScroll = function(scrollX, scrollY) {
	if(!this.canScroll) return;

	this.scrollX = scrollX;
	this.scrollY = scrollY;

	this.scrollX = _.clamp(this.scrollX, 0, this.boundX);
	this.scrollY = _.clamp(this.scrollY, 0, this.boundY);

	$(this).trigger("change");
};

Scroller.prototype.scroll = function(dx, dy) {
	this.setScroll(this.scrollX - dx, this.scrollY - dy);
};

Scroller.prototype.addBehavior = function(behavior) {
	this.behaviors.push(behavior);
	behavior.setScroller(this);
};

export default Scroller;
