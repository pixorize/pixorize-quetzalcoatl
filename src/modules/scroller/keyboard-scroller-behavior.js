import $ from 'jquery';

import ScrollerBehavior from './scroller-behavior';

var UP = 38;
var RIGHT = 39;
var DOWN = 40;
var LEFT = 37;

var KeyboardScrollerBehavior = function() {
	ScrollerBehavior.call(this);
};

KeyboardScrollerBehavior.prototype = Object.create(ScrollerBehavior.prototype);
KeyboardScrollerBehavior.prototype.constructor = KeyboardScrollerBehavior;

KeyboardScrollerBehavior.prototype.init = function() {
	this.SCROLL_SPEED = 10;
	this.SCROLL_STEP_INTERVAL = 20;

	this.upDown = false;
	this.rightDown = false;
	this.downDown = false;
	this.leftDown = false;

	$(document).on("keydown", this.__onDocumentKeyDown.bind(this));
	$(document).on("keyup", this.__onDocumentKeyUp.bind(this));
	setInterval(this.__step.bind(this), this.SCROLL_STEP_INTERVAL);
}

KeyboardScrollerBehavior.prototype.__step = function() {
	var dx = 0;
	var dy = 0;

	if(this.leftDown) dx ++;
	if(this.rightDown) dx --;
	if(this.upDown) dy ++;
	if(this.downDown) dy--;

	dx *= this.SCROLL_SPEED;
	dy *= this.SCROLL_SPEED;

	if(dx != 0 || dy != 0) {
		this.scroller.scroll(dx, dy);
	}
};

KeyboardScrollerBehavior.prototype.__onDocumentKeyDown = function(e) {
	if(document.activeElement != document.body) return;
	
	if(e.keyCode == UP) this.upDown = true;
	if(e.keyCode == RIGHT) this.rightDown = true;
	if(e.keyCode == DOWN) this.downDown = true;
	if(e.keyCode == LEFT) this.leftDown = true;

	if(this.upDown || this.rightDown || this.downDown || this.leftDown) {
		e.preventDefault();
	}
};

KeyboardScrollerBehavior.prototype.__onDocumentKeyUp = function(e) {
	if(e.keyCode == UP) this.upDown = false;
	if(e.keyCode == RIGHT) this.rightDown = false;
	if(e.keyCode == DOWN) this.downDown = false;
	if(e.keyCode == LEFT) this.leftDown = false;
};

export default KeyboardScrollerBehavior;
