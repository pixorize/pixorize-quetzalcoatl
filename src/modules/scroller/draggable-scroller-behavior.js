import $ from 'jquery';

import ScrollerBehavior from './scroller-behavior';

var DraggableScrollerBehavior = function(canvas) {
	ScrollerBehavior.call(this);

	var isMobile = /Mobi/.test(navigator.userAgent);
	if(!isMobile) {
		$(canvas).on("mousedown", this.__onCanvasMouseDown.bind(this));
		// $(canvas).mousedown(this.__onCanvasMouseDown.bind(this));
		$(document).mousemove(this.__onCanvasMouseMove.bind(this));
		$(document).mouseup(this.__onCanvasMouseUp.bind(this));
	} else {
		$(canvas).on("touchstart", this.__onCanvasTouchStart.bind(this));
		$(canvas).on("touchmove", this.__onCanvasTouchMove.bind(this));
		$(canvas).on("touchstop", this.__onCanvasTouchStop.bind(this));
	}
};

DraggableScrollerBehavior.prototype = Object.create(ScrollerBehavior.prototype);
DraggableScrollerBehavior.prototype.constructor = DraggableScrollerBehavior;

/*
 * startDrag for desktop and mobile
 */

DraggableScrollerBehavior.prototype.__onCanvasMouseDown = function(e) {
	this.__startDrag(e.pageX, e.pageY);
};

DraggableScrollerBehavior.prototype.__onCanvasTouchStart = function(e) {
	var touch = e.originalEvent.touches[0];
	this.__startDrag(touch.pageX, touch.pageY);
};

DraggableScrollerBehavior.prototype.__startDrag = function(x, y) {
	this.lastMouseX = x;
	this.lastMouseY = y;
	this.isDragging = true;
};

/*
 * stopDrag for desktop and mobile
 */

DraggableScrollerBehavior.prototype.__onCanvasMouseUp = function(e) {
	this.__stopDrag();
};

DraggableScrollerBehavior.prototype.__onCanvasTouchStop = function(e) {
	this.__stopDrag();
};

DraggableScrollerBehavior.prototype.__stopDrag = function() {
	this.isDragging = false;
};

/*
 * doDrag for desktop and mobile
 */

DraggableScrollerBehavior.prototype.__onCanvasMouseMove = function(e) {
	this.__doDrag(e.pageX, e.pageY);
}

DraggableScrollerBehavior.prototype.__onCanvasTouchMove = function(e) {
	var touch = e.originalEvent.touches[0];
	this.__doDrag(touch.pageX, touch.pageY);
}

DraggableScrollerBehavior.prototype.__doDrag = function(x, y) {
	this.mouseX = x;
	this.mouseY = y;

	if(!this.isDragging) return;

	var dx = this.mouseX - this.lastMouseX;
	var dy = this.mouseY - this.lastMouseY;

	this.dx = dx;
	this.dy = dy;

	this.scroller.scroll(this.dx, this.dy);

	this.lastMouseX = x;
	this.lastMouseY = y;
}

export default DraggableScrollerBehavior;
