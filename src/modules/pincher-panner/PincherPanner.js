import $ from 'jquery';
import Hammer from '@egjs/hammerjs';

function ePreventDefault(e) {
  e.preventDefault();
}

var PincherPanner = function(interactiveImage) {
	this.__initVars(interactiveImage);
	this.__setupHandlers();

  window.addEventListener('gesturestart', ePreventDefault);
  window.addEventListener('gesturechange', ePreventDefault);
  window.addEventListener('gestureend', ePreventDefault);
};

/*
* Init / Destroy
*/

PincherPanner.prototype.__initVars = function(interactiveImage) {
	this.interactiveImage = interactiveImage;
	this.isMobile = /Mobi|Tablet|iPad|iPhone/.test(navigator.userAgent);

	this.hasRecordedScale = false;
	this.origScale;
	this.origPos;
	this.imageCenter;

	this.hammertime = new Hammer(document.body, {
    domEvents: false,
    prevent_default: true,
    touchAction: 'none'
	});
};

PincherPanner.prototype.destroy = function() {
	this.__destroyHandlers();
};

/*
* Handlers
*/

PincherPanner.prototype.__setupHandlers = function() {
	if(!this.isMobile) return;

	this.hammertime.get('pinch').set({ enable: true });
	this.hammertime.on('pinch pan', this.__onPinchPan.bind(this));
	this.hammertime.on('pinchend panend', this.__onPinchPanEnd.bind(this));
};

PincherPanner.prototype.__destroyHandlers = function() {
	if(!this.isMobile) return;

  window.removeEventListener('gesturestart', ePreventDefault);
  window.removeEventListener('gesturechange', ePreventDefault);
  window.removeEventListener('gestureend', ePreventDefault);

	this.hammertime.off('pinch pan');
	this.hammertime.off('pinchend panend');
};

/*
* Logic
*/

PincherPanner.prototype.__withinBounds = function(e) {
  var canvas = $(this.interactiveImage.canvas);
  var offset = canvas.offset();

  var rect = {
    left: offset.left,
    top: offset.top,
    right: offset.left + canvas.width(),
    bottom: offset.top + canvas.height()
  };

  if(!(e.center.x > rect.left && e.center.x < rect.right)) {
    return false;
  }

  if(!(e.center.y > rect.top && e.center.y < rect.bottom)) {
    return false;
  }

  return true;
};

PincherPanner.prototype.__onPinchPan = function(e) {
  if(!this.__withinBounds(e)) return;

	if(!this.hasRecordedScale) {
		if(e.center.y >= $("footer").position().top) return;

		this.interactiveImage.scroller.canScroll = false;
		this.origScale = this.interactiveImage.zoomer.zoom;
		this.hasRecordedScale = true;
		this.imageCenter = this.interactiveImage.pageToImageCoords(e.center.x, e.center.y);
		this.origPos = {x: this.interactiveImage.scroller.scrollX, y: this.interactiveImage.scroller.scrollY}
	}

	this.interactiveImage.scroller.canScroll = true;
	if(e.scale != 1) {
		this.interactiveImage.zoomer.setZoom(this.origScale * e.scale);
	} else {
		this.interactiveImage.scroller.setScroll((this.origPos.x - e.deltaX)*e.scale, (this.origPos.y - e.deltaY)*e.scale);
	}
	this.interactiveImage.scroller.canScroll = false;

  e.preventDefault();
  return false;
};

PincherPanner.prototype.__onPinchPanEnd = function(e) {
	this.hasRecordedScale = false;
	this.interactiveImage.scroller.canScroll = false;

  e.preventDefault();
  return false;
};

export default PincherPanner;
